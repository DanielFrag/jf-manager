const path = require('path');
const { assert } = require('chai');
const Chance = require('chance');
const dataDriven = require('data-driven');
const request = require('supertest');
const sizeOf = require('image-size');
const app = require('../../lib/config/express-config');
const {
  REGISTRATION_VALUE,
  DAYS_TO_EXPIRE_DEBTS,
  USER_NAME,
  USER_PASS
} = require('../../lib/config/application-config');
const { ObjectId } = require('../../lib/infra/db');
const { dropCollectionsHelper } = require('../test-helpers');
const activityRepo = require('../../lib/repositories/activities-repository');
const debtsRepo = require('../../lib/repositories/debts-repository');
const clientRepo = require('../../lib/repositories/clients-repository');
const paymentsRepo = require('../../lib/repositories/payments-repository');
const authHeader = `Basic ${Buffer.from(`${USER_NAME}:${USER_PASS}`).toString('base64')}`;
const chance = new Chance();
describe('client-api functional tests', () => {
  const imageDir = path.resolve(__dirname, '../data');
  before(() => dropCollectionsHelper(['clients', 'activities', 'debts', 'payments']));
  afterEach(() => dropCollectionsHelper(['clients', 'activities', 'debts', 'payments']));
  describe('Success cases', () => {
    it('Should get the clients list, limited to 10 registers', async () => {
      await Promise.all(Array.from(new Array(20)).map(() => clientRepo.insertClient({
        birthDate: chance.pickone(['1986-10-21', '1986-10-22', '1986-09-21', '1986-11-21']),
        name: chance.name({
          middle: true
        }),
        activitiesData: []
      })));
      const result = await request(app)
        .get('/api/clients')
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.header['content-type'], 'application/json; charset=utf-8');
      assert.isTrue(Array.isArray(result.body));
      assert.strictEqual(result.body.length, 10);
      result.body.forEach(c => assert.containsAllKeys(c, ['_id', 'birthDate', 'name', 'activitiesData']));
    });
    it('Should get the sorted clients list, based on client name, limited to 10 registers', async () => {
      const auxArray = Array.from(new Array(20));
      const names = auxArray.map(() => `Foo ${chance.name()}`.toUpperCase());
      names.sort((a, b) => a > b ? -1 : 1);
      await names.reduce(
        (insertPromise, currentName) => insertPromise.then(() => clientRepo.insertClient({
          birthDate: chance.pickone(['1986-10-21', '1986-10-22', '1986-09-21', '1986-11-21']),
          name: currentName,
          activitiesData: []
        })),
        Promise.resolve()
      );
      const result = await request(app)
        .get('/api/clients?name=Foo&sort=name&sort=birthDate')
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.body.length, 10);
      names.sort();
      result.body.forEach((c, index) => {
        assert.containsAllKeys(c, ['_id', 'birthDate', 'name', 'activitiesData', 'debts', 'payments']);
        assert.strictEqual(c.name, names[index]);
      });
    });
    it('Should insert one client on db', async () => {
      const newClient = {
        birthDate: '1986-10-06',
        name: 'Sunda Foo Bar'
      };
      const result = await request(app)
        .post('/api/clients')
        .set('Authorization', authHeader)
        .send(newClient);
      assert.strictEqual(result.status, 201);
      assert.isOk(result.header['location']);
      const locationPathLevels = result.header['location'].split('/');
      const clientOnDB = await clientRepo.findOneByQuery({
        _id: new ObjectId(locationPathLevels[locationPathLevels.length - 1])
      });
      assert.strictEqual(Object.keys(clientOnDB).length, 6);
      assert.isOk(clientOnDB._id);
      assert.strictEqual(clientOnDB.name, newClient.name.toUpperCase());
      assert.deepEqual(clientOnDB.birthDate, new Date(newClient.birthDate));
      assert.deepEqual(clientOnDB.activitiesData, []);
      assert.typeOf(clientOnDB.registrationNumber, 'number');
      assert.strictEqual(clientOnDB.status, 'active');
      const debts = await debtsRepo
        .findByQuery({
          clientId: clientOnDB._id
        })
        .toArray();
      assert.strictEqual(debts.length, 1);
      const [registrationDebt] = debts;
      assert.strictEqual(registrationDebt.value, REGISTRATION_VALUE);
      assert.strictEqual(registrationDebt.type, 'registration');
      const refDate = new Date();
      assert.strictEqual(registrationDebt.generated.date.toLocaleDateString(),
        refDate.toLocaleDateString());
      refDate.setDate(refDate.getDate() + DAYS_TO_EXPIRE_DEBTS);
      assert.strictEqual(registrationDebt.details.dueDate.toLocaleDateString(),
        refDate.toLocaleDateString());
    });
    it('Should insert one client with complete data', async () => {
      const newClient = {
        birthDate: '1986-10-06',
        cpf: '219.706.502-57',
        debtsExpireDate: 5,
        name: 'Sunda Foo Bar',
        phone: '21988887777'
      };
      const result = await request(app)
        .post('/api/clients')
        .set('Authorization', authHeader)
        .send(newClient);
      assert.strictEqual(result.status, 201);
      const locationPathLevels = result.header['location'].split('/');
      const clientOnDB = await clientRepo.findOneByQuery({
        _id: new ObjectId(locationPathLevels[locationPathLevels.length - 1])
      });
      assert.strictEqual(clientOnDB.cpf, '21970650257');
      assert.strictEqual(clientOnDB.debtsExpireDate, newClient.debtsExpireDate);
      assert.strictEqual(clientOnDB.phone, newClient.phone);
    });
    it('Should calc the rigth registration number for a new client', async () => {
      const currentYear = new Date().getUTCFullYear();
      const regNumberBuilder = year => parseInt(`${year}${chance.string({
        length: 4,
        pool: '0123456789'
      })}`);
      const oldClientsRegistrationNumber = [
        regNumberBuilder(currentYear - 3), regNumberBuilder(currentYear - 3),
        regNumberBuilder(currentYear - 1), regNumberBuilder(currentYear - 1),
        regNumberBuilder(currentYear - 2), regNumberBuilder(currentYear - 2),
        regNumberBuilder(currentYear), regNumberBuilder(currentYear)
      ];
      await Promise.all(oldClientsRegistrationNumber.map(registrationNumber => clientRepo.insertClient({
        birthDate: chance.pickone(['1986-10-21', '1986-10-22', '1986-09-21', '1986-11-21']),
        name: chance.name({
          middle: true
        }),
        activitiesData: [],
        registrationNumber
      })));
      const newClient = {
        birthDate: '1986-10-06',
        name: 'Sunda Foo Bar'
      };
      const result = await request(app)
        .post('/api/clients')
        .set('Authorization', authHeader)
        .send(newClient);
      const locationPathLevels = result.header['location'].split('/');
      const clientOnDB = await clientRepo.findOneByQuery({
        _id: new ObjectId(locationPathLevels[locationPathLevels.length - 1])
      });
      assert.strictEqual(clientOnDB.registrationNumber, parseInt(`${currentYear}0003`));
    });
    it('Should update the client photo', async () => {
      const newClient = {
        birthDate: '1986-10-06',
        name: 'Sunda Foo Bar',
        activitiesData: []
      };
      const { insertedId } = await clientRepo.insertClient({
        birthDate: new Date(newClient.birthDate),
        name: newClient.name.toUpperCase()
      });
      const result = await request(app)
        .patch(`/api/clients/${insertedId.toHexString()}/image`)
        .set('Content-Type', 'multipart/form-data')
        .set('Authorization', authHeader)
        .attach('photo', `${imageDir}/image-example.png`);
      assert.strictEqual(result.status, 204);
      const updatedClient = await clientRepo.findOneByQuery({
        _id: insertedId
      });
      assert.isOk(updatedClient.photo);
      const photo = Buffer.from(updatedClient.photo, 'base64');
      const dimensions = sizeOf(photo);
      assert.deepEqual(dimensions, {
        width: 133,
        height: 150,
        type: 'png'
      });
    });
    it('Should update the client data', async () => {
      const newClient = {
        birthDate: '1986-10-06',
        name: 'Sunda Foo Bar'
      };
      const updateData = {
        cpf: '219.706.502-57',
        status: 'locked',
        phone: '21988887777',
        debtsExpireDate: 7
      };
      const { insertedId } = await clientRepo.insertClient({
        birthDate: new Date(newClient.birthDate),
        name: newClient.name.toUpperCase(),
        activitiesData: []
      });
      const result = await request(app)
        .patch(`/api/clients/${insertedId.toHexString()}`)
        .set('Authorization', authHeader)
        .send(updateData);
      assert.strictEqual(result.status, 204);
      const updatedClient = await clientRepo.findOneByQuery({
        _id: insertedId
      });
      assert.strictEqual(updatedClient.cpf, updateData.cpf.replace(/\.|-/gi, ''));
      assert.strictEqual(updatedClient.status, updateData.status);
      assert.strictEqual(updatedClient.phone, updateData.phone);
      assert.strictEqual(updatedClient.debtsExpireDate, updateData.debtsExpireDate);
    });
    it('Should keep the client data if there is nothing to update', async () => {
      const newClient = {
        birthDate: '1986-10-06',
        name: 'Sunda Foo Bar'
      };
      const { insertedId } = await clientRepo.insertClient({
        birthDate: new Date(newClient.birthDate),
        name: newClient.name.toUpperCase(),
        status: 'active',
        activitiesData: []
      });
      const result = await request(app)
        .patch(`/api/clients/${insertedId.toHexString()}`)
        .set('Authorization', authHeader)
        .send({});
      assert.strictEqual(result.status, 204);
      const updatedClient = await clientRepo.findOneByQuery({
        _id: insertedId
      });
      assert.isOk(updatedClient);
    });
    it('Should associate a valid activity for a client', async () => {
      const activityData = {
        name: chance.word(),
        instructor: chance.name(),
        values: {
          monthly: chance.natural(),
          single: chance.natural()
        }
      };
      const clientId = new ObjectId();
      await activityRepo.insertOne(activityData);
      await clientRepo.insertClient({
        _id: clientId,
        birthDate: chance.date({
          year: 1986
        }),
        name: chance.name(),
        activitiesData: [],
        registrationNumber: chance.natural()
      });
      const patchData = {
        name: activityData.name,
        customData: {
          belt: chance.color()
        }
      };
      const result = await request(app)
        .patch(`/api/clients/${clientId.toHexString()}/activities`)
        .set('Authorization', authHeader)
        .send(patchData);
      assert.strictEqual(result.status, 204, result.text);
      const clientEdited = await clientRepo.findOneByQuery({
        _id: clientId
      });
      assert.strictEqual(clientEdited.activitiesData.length, 1);
      assert.deepEqual(clientEdited.activitiesData[0].name, activityData.name);
      assert.deepEqual(clientEdited.activitiesData[0].customData, patchData.customData);
    });
    it('Should update the client activity custom data', async () => {
      const activityData = {
        name: chance.word({
          length: 20
        }),
        instructor: chance.name(),
        values: {
          monthly: chance.natural(),
          single: chance.natural()
        }
      };
      const clientId = new ObjectId();
      await activityRepo.insertOne(activityData);
      await clientRepo.insertClient({
        _id: clientId,
        birthDate: chance.date({
          year: 1986
        }),
        name: chance.name(),
        activitiesData: [{
          name: activityData.name,
          customData: {
            belt: chance.color()
          }
        }, {
          name: 'bobra',
          customData: {
            foo: 1234
          }
        }],
        registrationNumber: chance.natural()
      });
      const patchData = {
        name: activityData.name,
        customData: {
          belt: chance.color(),
          foo: 'bar',
          sunda: 9
        }
      };
      const result = await request(app)
        .patch(`/api/clients/${clientId.toHexString()}/activities`)
        .set('Authorization', authHeader)
        .send(patchData);
      assert.strictEqual(result.status, 204, result.text);
      const clientEdited = await clientRepo.findOneByQuery({
        _id: clientId
      });
      assert.strictEqual(clientEdited.activitiesData.length, 2);
      assert.deepEqual(clientEdited.activitiesData[0].name, activityData.name);
      assert.deepEqual(clientEdited.activitiesData[0].customData, patchData.customData);
      assert.deepEqual(clientEdited.activitiesData[1].name, 'bobra');
      assert.deepEqual(clientEdited.activitiesData[1].customData.foo, 1234);
    });
    it('Should remove the client activity custom data', async () => {
      const activityData = {
        name: chance.word({
          length: 20
        }),
        instructor: chance.name(),
        values: {
          monthly: chance.natural(),
          single: chance.natural()
        }
      };
      const clientId = new ObjectId();
      await activityRepo.insertOne(activityData);
      await clientRepo.insertClient({
        _id: clientId,
        birthDate: chance.date({
          year: 1986
        }),
        name: chance.name(),
        activitiesData: [{
          name: activityData.name,
          customData: {
            belt: chance.color()
          }
        }, {
          name: 'bar',
          customData: {
            sunda: 'bobra'
          }
        }],
        registrationNumber: chance.natural()
      });
      const patchData = {
        name: activityData.name
      };
      const result = await request(app)
        .patch(`/api/clients/${clientId.toHexString()}/activities`)
        .set('Authorization', authHeader)
        .send(patchData);
      assert.strictEqual(result.status, 204, result.text);
      const clientEdited = await clientRepo.findOneByQuery({
        _id: clientId
      });
      assert.strictEqual(clientEdited.activitiesData.length, 2);
      assert.deepEqual(clientEdited.activitiesData[0].name, activityData.name);
      assert.isNotOk(clientEdited.activitiesData[0].customData);
      assert.deepEqual(clientEdited.activitiesData[1].name, 'bar');
      assert.deepEqual(clientEdited.activitiesData[1].customData.sunda, 'bobra');
    });
    it('Should dissociate an activity of client', async () => {
      const activityName = 'Sunda Do - noite';
      const { insertedId: clientId } = await clientRepo.insertClient({
        birthDate: chance.date({
          year: 1986
        }),
        name: chance.name(),
        activitiesData: [{
          name: activityName,
          customData: {
            foo: 'bar'
          }
        }],
        registrationNumber: chance.natural()
      });
      const result = await request(app)
        .delete(`/api/clients/${clientId.toHexString()}/activities/${activityName}`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 204);
      const clientEdited = await clientRepo.findOneByQuery({
        _id: clientId
      });
      assert.strictEqual(clientEdited.activitiesData.length, 0);
    });
    describe('Get client using unique fields', () => {
      const clientData = {
        registrationNumber: 999,
        _id: null
      };
      const newClient = {
        birthDate: '1986-10-06',
        name: 'Sunda Foo Bar'
      };
      beforeEach(async () => {
        const { insertedId } = await clientRepo.insertClient({
          birthDate: new Date(newClient.birthDate),
          name: newClient.name.toUpperCase(),
          activitiesData: [],
          registrationNumber: clientData.registrationNumber
        });
        clientData._id = insertedId.toHexString();
      });
      it('Should get a client using its id', async () => {
        const result = await request(app)
          .get(`/api/clients/${clientData._id}`)
          .set('Authorization', authHeader);
        assert.strictEqual(result.status, 200);
        assert.strictEqual(result.body.name, newClient.name.toUpperCase());
        assert.strictEqual(result.body.birthDate, new Date(newClient.birthDate).toISOString());
      });
      it('Should get a client using its registrationNumber', async () => {
        const result = await request(app)
          .get(`/api/clients?registrationNumber=${clientData.registrationNumber}`)
          .set('Authorization', authHeader);
        assert.strictEqual(result.status, 200);
        const [client] = result.body;
        assert.strictEqual(client.name, newClient.name.toUpperCase());
        assert.strictEqual(client.birthDate, new Date(newClient.birthDate).toISOString());
      });
    });
    describe('Get full client data', () => {
      const name = chance.name().toUpperCase();
      const registrationNumber = chance.natural();
      const paymentId = new ObjectId();
      const debtsIds = {
        closed: null,
        open: null
      };
      beforeEach(async () => {
        const activityId = new ObjectId();
        const clientData = {
          birthDate: chance.date(),
          name,
          activitiesData: [{
            id: activityId,
            customData: {
              foo: chance.word()
            }
          }],
          registrationNumber
        };
        const { insertedId: clientId } = await clientRepo.insertClient(clientData);
        const debtClosed = {
          clientId,
          details: {
            referenceDate: chance.date({})
          },
          generated: {
            adminId: chance.word(),
            date: chance.date()
          },
          paymentId,
          activityId: activityId,
          type: 'registration',
          value: chance.natural()
        };
        const openDebt = {
          clientId,
          details: {
            referenceDate: chance.date(),
            dueDate: chance.date()
          },
          generated: {
            adminId: 'scheduler',
            date: chance.date()
          },
          activityId: activityId,
          type: 'monthly payment',
          value: chance.natural()
        };
        const { insertedId: debtClosedId} = await debtsRepo.insertOne(debtClosed);
        debtsIds.closed = debtClosedId;
        const { insertedId: openDebtId} = await debtsRepo.insertOne(openDebt);
        debtsIds.open = openDebtId;
        const paymentData = {
          _id: paymentId,
          adminId: chance.word(),
          data: chance.date(),
          debtsData: [{
            id: debtClosedId,
            valueFactor: chance.floating({
              min: 0
            })
          }],
          total: debtClosed.value
        };
        await paymentsRepo.insertOne(paymentData);
      });
      dataDriven([{
        qsParam: 'registrationNumber',
        data: registrationNumber
      }, {
        qsParam: 'name',
        data: name.toLowerCase()
      }], () => {
        it('Should get the client with his debts and payments with his {qsParam}', async (ctx) => {
          const urlPath = `/api/clients?${ctx.qsParam}=${ctx.data}`;
          const result = await request(app)
            .get(urlPath)
            .set('Authorization', authHeader);
          assert.strictEqual(result.status, 200);
          assert.lengthOf(result.body, 1);
          const [client] = result.body;
          assert.isTrue(Array.isArray(client.debts));
          assert.strictEqual(client.debts.length, 2);
          assert.strictEqual(client.debts[0]._id, debtsIds.closed.toHexString());
          assert.strictEqual(client.debts[0].paymentId, paymentId.toHexString());
          assert.strictEqual(client.debts[1]._id, debtsIds.open.toHexString());
          assert.isNotOk(client.debts[1].paymentId);
          assert.isTrue(Array.isArray(client.payments));
          assert.strictEqual(client.payments.length, 1);
          assert.strictEqual(client.payments[0]._id, paymentId.toHexString());
        });
      });
    });
  });
  describe('Error cases', () => {
    it('Should return conflict on post client with a name and bith date that already exists', async () => {
      const newClient = {
        birthDate: '1986-10-06',
        name: 'Sunda Foo Bar',
        activitiesData: []
      };
      await clientRepo.insertClient({
        birthDate: new Date(newClient.birthDate),
        name: newClient.name.toUpperCase()
      });
      const result = await request(app)
        .post('/api/clients')
        .set('Authorization', authHeader)
        .send(newClient);
      assert.strictEqual(result.status, 409);
      const clientsOnDB = await clientRepo.findByQuery({}).toArray();
      assert.strictEqual(clientsOnDB.length, 1);
      assert.strictEqual(clientsOnDB[0].name, newClient.name.toUpperCase());
      assert.deepEqual(clientsOnDB[0].birthDate, new Date(newClient.birthDate));
    });
    describe('Should return bad request for post client with:', () => {
      dataDriven([{
        description: 'invalid cpf',
        newClient: {
          birthDate: '1986-10-06',
          name: 'Sunda Foo Bar',
          cpf: '347.496.221-34'
        },
        errorMessage: 'The cpf is invalid'
      }, {
        description: 'invalid birth dates',
        newClient: {
          birthDate: `${new Date().getUTCFullYear() + 1}-12-31`,
          name: 'Sunda Foo Bar',
          cpf: '347.496.221-33'
        },
        errorMessage: 'The birth date is invalid'
      }], () => {
        it('{description}', async (ctx) => {
          const result = await request(app)
            .post('/api/clients')
            .set('Authorization', authHeader)
            .send(ctx.newClient);
          assert.strictEqual(result.status, 400);
          assert.strictEqual(result.text, ctx.errorMessage);
        });
      });
    });
    it('Should return bad request for post invalid client object', async () => {
      const newClient = {
        birthDate: '1986-10-06',
        cpf: '1'
      };
      const result = await request(app)
        .post('/api/clients')
        .set('Authorization', authHeader)
        .send(newClient);
      assert.strictEqual(result.status, 400);
      assert.isTrue(Array.isArray(result.body));
      assert.strictEqual(result.body.length, 2);
      assert.deepEqual(result.body, [{
        message: "Missing required property: name",
        path: "/required/1"
      }, {
        message: "Format validation failed (Invalid CPF string)",
        path: "/properties/cpf/format"
      }]);
    });
    it('Should return bad request when try to update a photo for a client that not exists', async () => {
      const result = await request(app)
        .patch('/api/clients/5c8871a99a7b2f15c4e7769b/image')
        .set('Content-Type', 'multipart/form-data')
        .set('Authorization', authHeader)
        .attach('photo', `${imageDir}/image-example.png`);
      assert.strictEqual(result.status, 404);
    });
    it('Should return bad request when try to associate an inexistent activity for a client', async () => {
      const { insertedId: clientId } = await clientRepo.insertClient({
        birthDate: chance.date({
          year: 1986
        }),
        name: chance.name(),
        activitiesData: [],
        registrationNumber: chance.natural()
      });
      const activityData = {
        name: chance.word(),
        customData: {
          belt: chance.color()
        }
      };
      const result = await request(app)
        .patch(`/api/clients/${clientId.toHexString()}/activities`)
        .set('Authorization', authHeader)
        .send(activityData);
      assert.strictEqual(result.status, 400);
      const clientOnDb = await clientRepo.findOneByQuery({
        _id: clientId
      });
      assert.strictEqual(clientOnDb.activitiesData.length, 0);
    });
    it('Should return not found when try dissociate an activity that the client does not have', async () => {
      const fooActivity = {
        name: 'foo',
        customData: {
          foo: 'bar'
        }
      };
      const { insertedId: clientId } = await clientRepo.insertClient({
        birthDate: chance.date({
          year: 1986
        }),
        name: chance.name(),
        activitiesData: [fooActivity],
        registrationNumber: chance.natural()
      });
      const result = await request(app)
        .delete(`/api/clients/${clientId.toHexString()}/activities/bar`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 404);
      const clientEdited = await clientRepo.findOneByQuery({
        _id: clientId
      });
      assert.strictEqual(clientEdited.activitiesData.length, 1);
      assert.deepEqual(clientEdited.activitiesData[0], fooActivity);
    });
    it('Should return unauthorized for requests without authorization header', async () => {
      const result = await request(app)
        .post('/api/clients')
        .send({});
      assert.strictEqual(result.status, 401);
    });
    it('Should return unauthorized for requests with wrong data on authorization header', async () => {
      const result = await request(app)
        .post('/api/clients')
        .set('Authorization', `${authHeader}asd`)
        .send({});
      assert.strictEqual(result.status, 401);
    });
  });
});