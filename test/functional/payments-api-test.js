const { assert } = require('chai');
const request = require('supertest');
const dataBuilder = require('../data/data-builder');
const {
  USER_NAME,
  USER_PASS
} = require('../../lib/config/application-config');
const app = require('../../lib/config/express-config');
const { ObjectId } = require('../../lib/infra/db');
const { dropCollectionsHelper } = require('../test-helpers');
const debtsRepo = require('../../lib/repositories/debts-repository');
const clientsRepo = require('../../lib/repositories/clients-repository');
const paymentsRepo = require('../../lib/repositories/payments-repository');
const authHeader = `Basic ${Buffer.from(`${USER_NAME}:${USER_PASS}`).toString('base64')}`;
describe('payments api test', () => {
  before(async () => await dropCollectionsHelper(['debts', 'payments', 'clients']));
  afterEach(async () => await dropCollectionsHelper(['debts', 'payments', 'clients']));
  describe('Success cases', () => {
    it('Should create a payment and close the related debts', async () => {
      const debtOne = dataBuilder.debt();
      const debtTwo = dataBuilder.debt();
      const { insertedId: debtOneId } = await debtsRepo.insertOne(debtOne);
      const { insertedId: debtTwoId } = await debtsRepo.insertOne(debtTwo);
      const postData = [{
        id: debtOneId.toHexString()
      }, {
        id: debtTwoId.toHexString(),
        valueFactor: 0.8
      }];
      const result = await request(app)
        .post('/api/payments')
        .set('Authorization', authHeader)
        .send(postData);
      assert.strictEqual(result.status, 201);
      assert.isOk(result.header['location']);
      assert.include(result.header['location'], '/api/payments/');
      const locationData = result.header['location'].split('/');
      const paymentId = new ObjectId(locationData[locationData.length - 1]);
      const paymentDB = await paymentsRepo.findOneByQuery({
        _id: paymentId
      });
      assert.isOk(paymentDB);
      assert.isOk(paymentDB.adminId);
      assert.strictEqual(paymentDB.date.toLocaleDateString(), new Date().toLocaleDateString());
      const debtsData = postData.map(item => ({
        id: new ObjectId(item.id),
        valueFactor: item.valueFactor || 1
      }));
      assert.deepEqual(paymentDB.debtsData, debtsData);
      assert.strictEqual(paymentDB.total, debtOne.value + debtTwo.value * 0.8);
      const debts = await debtsRepo
        .findByQuery({
          _id: {
            $in: [debtOneId, debtTwoId]
          }
        })
        .toArray();
      assert.deepEqual(debts[0].paymentId, paymentDB._id);
      assert.deepEqual(debts[1].paymentId, paymentDB._id);
    });
    it('Should get the payments list with debts detailed', async () => {
      const cOne = dataBuilder.client({
        registrationNumber: 1
      });
      const cTwo = dataBuilder.client({
        registrationNumber: 2
      });
      const cThree = dataBuilder.client({
        registrationNumber: 3
      });
      await Promise.all([cOne, cTwo, cThree].map(c => clientsRepo.insertClient(c)));
      const dOne = dataBuilder.debt({
        clientId: cOne._id,
        value: 50
      });
      const dTwo = dataBuilder.debt({
        clientId: cOne._id
      });
      const dThree = dataBuilder.debt({
        clientId: cTwo._id,
        value: 80
      });
      const dFour = dataBuilder.debt({
        clientId: cThree._id,
        value: 60
      });
      await Promise.all([dOne, dTwo, dThree, dFour].map(d => debtsRepo.insertOne(d)));
      await Promise.all([[{
        id: dOne._id.toHexString()
      }, {
        id: dThree._id.toHexString(),
        valueFactor: 0.8
      }], [{
        id: dFour._id.toHexString()
      }]].map(body => request(app)
        .post('/api/payments')
        .set('Authorization', authHeader)
        .send(body)));
      const result = await request(app)
        .get('/api/payments')
        .set('Authorization', authHeader);
      assert.lengthOf(result.body, 2);
      const paymentForDebtOneAndThree = result.body.find(p => p.total === 114);
      assert.isOk(paymentForDebtOneAndThree);
      assert.deepEqual(paymentForDebtOneAndThree.debtsData, [{
        id: dOne._id.toHexString(),
        valueFactor: 1
      }, {
        id: dThree._id.toHexString(),
        valueFactor: 0.8
      }]);
      assert.lengthOf(paymentForDebtOneAndThree.debts, 2);
      const debtOneDetail = paymentForDebtOneAndThree.debts.find(d => d._id === dOne._id.toHexString());
      assert.isOk(debtOneDetail);
      assert.strictEqual(debtOneDetail.client.registrationNumber, cOne.registrationNumber);
      const debtThreeDetail = paymentForDebtOneAndThree.debts.find(d => d._id === dThree._id.toHexString());
      assert.isOk(debtThreeDetail);
      assert.strictEqual(debtThreeDetail.client.registrationNumber, cTwo.registrationNumber);
      const paymentForDebtFour = result.body.find(p => p.total === 60);
      assert.isOk(paymentForDebtFour);
      assert.deepEqual(paymentForDebtFour.debtsData, [{
        id: dFour._id.toHexString(),
        valueFactor: 1
      }]);
      assert.lengthOf(paymentForDebtFour.debts, 1);
      const debtFourDetail = paymentForDebtFour.debts.find(d => d._id === dFour._id.toHexString());
      assert.isOk(debtFourDetail);
      assert.strictEqual(debtFourDetail.client.registrationNumber, cThree.registrationNumber);
    });
  });
  describe('Error cases', () => {
    it('Should return pre condition failed if the one of the payment\'s debt not exists', async () => {
      const validDebt = dataBuilder.debt();
      const phantonDebt = dataBuilder.debt({
        _id: new ObjectId()
      });
      const { insertedId: validDebtId } = await debtsRepo.insertOne(validDebt);
      const result = await request(app)
        .post('/api/payments')
        .set('Authorization', authHeader)
        .send([{
          id: validDebtId.toHexString()
        }, {
          id: phantonDebt._id.toHexString()
        }]);
      assert.strictEqual(result.status, 412);
      assert.strictEqual(result.text, 'Could not found all the requested debts');
      const validDebtDB = await debtsRepo.findOneByQuery({
        _id: validDebtId
      });
      assert.isNotOk(validDebtDB.paymentId);
    });
    it('Should return pre condition failed if the one of the payment\'s debt was already paid', async () => {
      const debtOne = dataBuilder.debt({
        paymentId: new ObjectId()
      });
      const debtTwo = dataBuilder.debt();
      const { insertedId: debtOneId } = await debtsRepo.insertOne(debtOne);
      const { insertedId: debtTwoId } = await debtsRepo.insertOne(debtTwo);
      const result = await request(app)
        .post('/api/payments')
        .set('Authorization', authHeader)
        .send([{
          id: debtOneId.toHexString()
        }, {
          id: debtTwoId.toHexString()
        }]);
      assert.strictEqual(result.status, 412);
      assert.strictEqual(result.text, 'Some debt was already paid');
      const debtsOneDB = await debtsRepo.findOneByQuery({
        _id: debtOneId
      });
      const debtTwoDB = await debtsRepo.findOneByQuery({
        _id: debtTwoId
      });
      assert.deepEqual(debtsOneDB.paymentId, debtOne.paymentId);
      assert.isNotOk(debtTwoDB.paymentId);
    });
  });
});