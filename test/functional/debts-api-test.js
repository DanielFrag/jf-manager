const { assert } = require('chai');
const Chance = require('chance');
const dataDriven = require('data-driven');
const request = require('supertest');
const dataBuilder = require('../data/data-builder');
const {
  USER_NAME,
  USER_PASS
} = require('../../lib/config/application-config');
const app = require('../../lib/config/express-config');
const { ObjectId } = require('../../lib/infra/db');
const { dropCollectionsHelper } = require('../test-helpers');
const activityRepo = require('../../lib/repositories/activities-repository');
const clientRepo = require('../../lib/repositories/clients-repository');
const debtsRepo = require('../../lib/repositories/debts-repository');
const referenceMonthBuilder = require('../../lib/utils/build-reference-month');
const authHeader = `Basic ${Buffer.from(`${USER_NAME}:${USER_PASS}`).toString('base64')}`;
const chance = new Chance();
const now = new Date();
const refMonth = referenceMonthBuilder(now);
const buildActivity = (name) => dataBuilder.activity({
  _id: new ObjectId(),
  name
});
const activityFooData = buildActivity('foo');
const activityBarData = buildActivity('bar');
const phantonActivity = buildActivity('phanton');
const clientData = dataBuilder.client({
  _id: new ObjectId(),
  activitiesData: [{
    name: activityFooData.name,
    customData: {
      belt: chance.color()
    }
  }, {
    name: phantonActivity.name
  }]
});
describe('debts api tests', () => {
  before(async () => {
    await dropCollectionsHelper(['clients', 'activities', 'debts', 'payments']);
    await activityRepo.insertOne(activityFooData);
    await activityRepo.insertOne(activityBarData);
    await clientRepo.insertClient(clientData);
  });
  afterEach(async () => {
    await dropCollectionsHelper(['debts', 'payments']);
  });
  after(async () => {
    await dropCollectionsHelper(['clients', 'activities', 'debts', 'payments']);
  });
  describe('Success cases', () => {
    it('Should insert a debt on DB', async () => {
      const dueDate = new Date(now);
      dueDate.setDate(now.getDate() + 2);
      const formatedMonth = (1 + dueDate.getUTCMonth()).toString().padStart(2, '0');
      const formatedDate = (1 + dueDate.getUTCDate()).toString().padStart(2, '0');
      const formatedDueDate = `${dueDate.getUTCFullYear()}-${formatedMonth}-${formatedDate}`;
      const result = await request(app)
        .post('/api/debts')
        .set('Authorization', authHeader)
        .send({
          activityName: activityFooData.name,
          clientId: clientData._id.toHexString(),
          dueDate: formatedDueDate,
          referenceMonth: refMonth
        });
      assert.strictEqual(result.status, 201, result.text);
      assert.isOk(result.header['location']);
      const locationData = result.header['location'].split('/');
      const debtId = new ObjectId(locationData[locationData.length - 1]);
      const debtDb = await debtsRepo.findOneByQuery({
        _id: debtId
      });
      assert.isOk(debtDb);
      assert.deepEqual(debtDb.clientId, clientData._id);
      assert.deepEqual(debtDb.activityName, activityFooData.name);
      assert.strictEqual(debtDb.details.referenceMonth, refMonth);
      assert.strictEqual(debtDb.value, activityFooData.values.monthly);
      assert.strictEqual(debtDb.type, 'monthly payment');
      assert.deepEqual(debtDb.details.dueDate, new Date(formatedDueDate));
      assert.strictEqual(debtDb.generated.date.toLocaleDateString(), now.toLocaleDateString());
    });
    it('Should set the due date for current day plus one of reference month,'
      + ' if due date is not sent', async () => {
      const refMonth = new Date();
      refMonth.setMonth(now.getMonth() + 1);
      const referenceDueDate = new Date(now);
      referenceDueDate.setDate(now.getDate() + 1);
      referenceDueDate.setMonth(now.getMonth() + 1);
      const result = await request(app)
        .post('/api/debts')
        .set('Authorization', authHeader)
        .send({
          activityName: activityFooData.name,
          clientId: clientData._id.toHexString(),
          referenceMonth: referenceMonthBuilder(refMonth)
        });
      assert.strictEqual(result.status, 201, result.text);
      const locationData = result.header['location'].split('/');
      const debtId = new ObjectId(locationData[locationData.length - 1]);
      const debtDb = await debtsRepo.findOneByQuery({
        _id: debtId
      });
      assert.strictEqual(debtDb.details.dueDate.toDateString(), referenceDueDate.toDateString());
    });
    it('Should get a debt by its id', async () => {
      const debtData = dataBuilder.debt({
        paymentId: new ObjectId()
      });
      const { insertedId } = await debtsRepo.insertOne(debtData);
      const result = await request(app)
        .get(`/api/debts/${insertedId.toHexString()}`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.body.activityName, debtData.activityName);
      assert.strictEqual(result.body.clientId, debtData.clientId.toHexString());
      assert.strictEqual(result.body.details.annotations, debtData.details.annotations);
      assert.strictEqual(result.body.details.dueDate, debtData.details.dueDate.toISOString());
      assert.strictEqual(result.body.details.referenceMonth, debtData.details.referenceMonth);
      assert.strictEqual(result.body.paymentId, debtData.paymentId.toHexString());
      assert.strictEqual(result.body.value, debtData.value);
      assert.strictEqual(result.body.type, debtData.type);
      assert.strictEqual(result.body.generated.date, debtData.generated.date.toISOString());
    });
  });
  describe('Error cases', () => {
    it('Should keep the debt uniqueness (clientId, activityId and referenceMonth),'
      + ' returning conflit when the constraint is not respected', async () => {
      const debtData = dataBuilder.debt({
        clientId: clientData._id,
        activityName: activityFooData.name,
        details: {
          referenceMonth: refMonth
        }
      });
      await debtsRepo.insertOne(debtData);
      const result = await request(app)
        .post('/api/debts')
        .set('Authorization', authHeader)
        .send({
          activityName: debtData.activityName,
          clientId: debtData.clientId.toHexString(),
          referenceMonth: refMonth
        });
      assert.strictEqual(result.status, 409);
    });
    describe('Error on post debt with wrong conditions', async () => {
      dataDriven([{
        description: 'Client not exists',
        bodyData: {
          activityName: activityFooData.name,
          clientId: new ObjectId().toHexString(),
          referenceMonth: refMonth
        },
        errorMessage: 'The client not exists'
      }, {
        description: 'Activity not exists',
        bodyData: {
          activityName: phantonActivity.name,
          clientId: clientData._id.toHexString(),
          referenceMonth: refMonth
        },
        errorMessage: 'Could not found the activity data'
      }, {
        description: 'Activity not associated to client',
        bodyData: {
          activityName: activityBarData.name,
          clientId: clientData._id.toHexString(),
          referenceMonth: refMonth
        },
        errorMessage: 'The activity is not associated to client'
      }], () => {
        it('{description}', async (ctx) => {
          const result = await request(app)
            .post('/api/debts')
            .set('Authorization', authHeader)
            .send(ctx.bodyData);
          assert.strictEqual(result.status, 412);
          assert.strictEqual(result.text, ctx.errorMessage);
        });
      });
    });
    it('Should return not found for a debt that does not exists', async () => {
      const debtId = new ObjectId();
      const result = await request(app)
        .get(`/api/debts/${debtId.toHexString()}`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 404);
    });
  });
});
