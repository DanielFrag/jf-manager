const { assert } = require('chai');
const Chance = require('chance');
const request = require('supertest');
const sinon = require('sinon');
const app = require('../../lib/config/express-config');
const dataBuilder = require('../data/data-builder');
const { dropCollectionsHelper } = require('../test-helpers');
const {
  DEFAULT_EXPIRE_DATE,
  USER_NAME,
  USER_PASS
} = require('../../lib/config/application-config');
const activitiesRepo = require('../../lib/repositories/activities-repository');
const clientsRepo = require('../../lib/repositories/clients-repository');
const debtsRepo = require('../../lib/repositories/debts-repository');
const chance = new Chance();
const authHeader = `Basic ${Buffer.from(`${USER_NAME}:${USER_PASS}`).toString('base64')}`;
const activitiesMap = new Map();
const clientsMap = new Map();
const populateDB = async () => {
  activitiesMap.set('foo', dataBuilder.activity({
    name: 'foo'
  }));
  activitiesMap.set('bar', dataBuilder.activity({
    name: 'bar'
  }));
  await activitiesRepo.insertOne(activitiesMap.get('foo'));
  await activitiesRepo.insertOne(activitiesMap.get('bar'));
  clientsMap.set('FOO', dataBuilder.client({
    name: 'FOO',
    activitiesData: [{
      name: activitiesMap.get('foo').name
    }],
    debtsExpireDate: chance.natural({
      min: 1,
      max: 28,
      exclude: [DEFAULT_EXPIRE_DATE]
    })
  }));
  clientsMap.set('BAR', dataBuilder.client({
    name: 'BAR',
    activitiesData: [{
      name: activitiesMap.get('bar').name
    }]
  }));
  clientsMap.set('SUNDA', dataBuilder.client({
    name: 'SUNDA'
  }));
  clientsMap.set('BROBA', dataBuilder.client({
    name: 'BOBRA',
    activitiesData: [{
      name: activitiesMap.get('foo').name
    }, {
      name: activitiesMap.get('bar').name
    }]
  }));
  clientsMap.set('LOCKED', dataBuilder.client({
    name: 'LOCKED',
    activitiesData: [{
      name: activitiesMap.get('foo').name
    }, {
      name: activitiesMap.get('bar').name
    }],
    status: 'locked'
  }));
  clientsMap.set('INACTIVE', dataBuilder.client({
    name: 'INACTIVE',
    activitiesData: [{
      name: activitiesMap.get('foo').name
    }],
    status: 'inactive'
  }));
  const promiseArray = [];
  clientsMap.forEach(c => promiseArray.push(clientsRepo.insertClient(c)));
  await Promise.all(promiseArray);
};
const assertData = async (referenceMonth) => {
  const debts = await debtsRepo
    .findByQuery({
      'details.referenceMonth': referenceMonth
    })
    .toArray();
  assert.strictEqual(debts.length, 4);
  const activityFoo = activitiesMap.get('foo');
  const activityBar = activitiesMap.get('bar');
  const fooActivityDebts = debts.filter(d => d.value === activityFoo.values.monthly
    && d.activityName === activityFoo.name);
  const barActivityDebts = debts.filter(d => d.value === activityBar.values.monthly
    && d.activityName === activityBar.name);
  assert.strictEqual(fooActivityDebts.length, 2);
  assert.strictEqual(barActivityDebts.length, 2);
  const clientFooDebts = debts.filter(d => d.value === activityFoo.values.monthly
    && d.clientId.toHexString() === clientsMap.get('FOO')._id.toHexString());
  assert.strictEqual(clientFooDebts.length, 1);
  assert.strictEqual(clientFooDebts[0].details.dueDate.getUTCDate(), clientsMap.get('FOO').debtsExpireDate);
  const clientBarDebts = debts.filter(d => d.value === activityBar.values.monthly
    && d.clientId.toHexString() === clientsMap.get('BAR')._id.toHexString());
  assert.strictEqual(clientBarDebts.length, 1);
  assert.strictEqual(clientBarDebts[0].details.dueDate.getUTCDate(), DEFAULT_EXPIRE_DATE);
  const clientSundaDebts = debts.find(d => 
    d.clientId.toHexString() === clientsMap.get('SUNDA')._id.toHexString());
  assert.isNotOk(clientSundaDebts);
  const clientBrobaDebts = debts.filter(d => (d.value === activityBar.values.monthly
    || d.value === activityFoo.values.monthly)
    && d.clientId.toHexString() === clientsMap.get('BROBA')._id.toHexString());
  assert.strictEqual(clientBrobaDebts.length, 2);
  const clientLockedDebts = debts.filter(
    d => d.clientId.toHexString() === clientsMap.get('LOCKED')._id.toHexString()
  );
  assert.strictEqual(clientLockedDebts.length, 0);
  const clientInactiveDebts = debts.filter(
    d => d.clientId.toHexString() === clientsMap.get('INACTIVE')._id.toHexString()
  );
  assert.strictEqual(clientInactiveDebts.length, 0);
};
describe('tasks api test', () => {
  before(async () => {
    await dropCollectionsHelper(['activities', 'clients']);
    await populateDB();
  });
  beforeEach(async () => {
    await dropCollectionsHelper(['debts']);
  });
  after(async () => {
    await dropCollectionsHelper(['activities', 'clients', 'debts']);
  });
  it('Should create the monthly debts, based on reference month,'
    + ' for clients that has a valid activity', (done) => {
    const referenceMonth = '2019-03';
    const consoleStub = sinon
      .stub(console, 'log')
      .callsFake((message) => {
        consoleStub.restore();
        assert.strictEqual(message, 'All monthly debts were generated');
        assertData(referenceMonth).then(() => done());
      });
    request(app)
      .post('/api/tasks/generate-monthly-debts')
      .set('Authorization', authHeader)
      .send({
        referenceMonth
      })
      .end((err, result) => {
        assert.isNotOk(err);
        assert.strictEqual(result.status, 202);
      });
  });
  it('Should create the monthly debts without duplicates', (done) => {
    const referenceMonth = '2019-03';
    const fooClient = clientsMap.get('FOO');
    const previousDebt = dataBuilder.debt({
      activityName: activitiesMap.get('foo').name,
      clientId: fooClient._id,
      details: {
        referenceMonth,
        dueDate: new Date(2019, 2, fooClient.debtsExpireDate)
      },
      value: activitiesMap.get('foo').values.monthly
    });
    debtsRepo
      .insertOne(previousDebt)
      .then(() => {
        const consoleStub = sinon
          .stub(console, 'log')
          .callsFake((message) => {
            consoleStub.restore();
            assert.strictEqual(message, 'All monthly debts were generated');
            assertData(referenceMonth).then(() => done());
          });
        request(app)
          .post('/api/tasks/generate-monthly-debts')
          .set('Authorization', authHeader)
          .send({
            referenceMonth
          })
          .end((err, result) => {
            assert.isNotOk(err);
            assert.strictEqual(result.status, 202);
          });
      });
  });
});
