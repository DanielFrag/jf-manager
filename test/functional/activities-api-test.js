const { assert } = require('chai');
const Chance = require('chance');
const request = require('supertest');
const {
  USER_NAME,
  USER_PASS
} = require('../../lib/config/application-config');
const app = require('../../lib/config/express-config');
const dataBuilder = require('../data/data-builder');
const { ObjectId } = require('../../lib/infra/db');
const { dropCollectionsHelper } = require('../test-helpers');
const activitiesRepo = require('../../lib/repositories/activities-repository');
const authHeader = `Basic ${Buffer.from(`${USER_NAME}:${USER_PASS}`).toString('base64')}`;
const chance = new Chance();
describe('Activities api test', () => {
  beforeEach(() => dropCollectionsHelper(['activities']));
  after(() => dropCollectionsHelper(['activities']));
  describe('Success cases', () => {
    it('Should list the first 10 activities', async () => {
      await Promise.all(Array.from(new Array(20)).map(() => activitiesRepo.insertOne(dataBuilder.activity())));
      const result = await request(app)
        .get('/api/activities')
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.header['content-type'], 'application/json; charset=utf-8');
      assert.isTrue(Array.isArray(result.body));
      assert.strictEqual(result.body.length, 10);
      result.body.forEach(c => assert.containsAllKeys(c, ['instructor', 'name', 'timesheet', 'values']));
    });
    it('Should create an activity', async () => {
      const activityData = dataBuilder.activity();
      delete activityData._id;
      const result = await request(app)
        .post('/api/activities')
        .set('Authorization', authHeader)
        .send(activityData);
      assert.strictEqual(result.status, 201);
      assert.isOk(result.header['location']);
      const locationPathLevels = result.header['location'].split('/');
      const newActivityId = locationPathLevels.pop();
      const activityOnDb = await activitiesRepo.findOneByQuery({
        _id: new ObjectId(newActivityId)
      });
      assert.isOk(activityOnDb);
      delete activityOnDb._id;
      assert.deepEqual(activityOnDb, activityData);
    });
    it('Should delete an activity', async () => {
      const { insertedId } = await activitiesRepo.insertOne(dataBuilder.activity());
      const result = await request(app)
        .delete(`/api/activities/${insertedId.toHexString()}`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 204);
      const deletedActivity = await activitiesRepo.findOneByQuery({
        _id: insertedId
      });
      assert.isNotOk(deletedActivity);
    });
    it('Should update the activity data', async () => {
      const activityData = dataBuilder.activity();
      const { insertedId } = await activitiesRepo.insertOne(activityData);
      delete activityData._id;
      activityData.timesheet.push(dataBuilder.activitiesSchedule());
      activityData.instructor = chance.name();
      const result = await request(app)
        .put(`/api/activities/${insertedId.toHexString()}`)
        .set('Authorization', authHeader)
        .send(activityData);
      assert.strictEqual(result.status, 204);
      const activityOnDb = await activitiesRepo.findOneByQuery({
        _id: insertedId
      });
      delete activityOnDb._id;
      assert.deepEqual(activityOnDb, activityData);
    });
    it('Should get an activity by its id', async () => {
      const activityData = dataBuilder.activity();
      const { insertedId } = await activitiesRepo.insertOne(activityData);
      const result = await request(app)
        .get(`/api/activities/${insertedId.toHexString()}`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.body.name, activityData.name);
      assert.strictEqual(result.body.instructor, activityData.instructor);
      assert.deepEqual(result.body.values, activityData.values);
    });
    it('Should get an activity by its name', async () => {
      const activityData = dataBuilder.activity();
      await activitiesRepo.insertOne(activityData);
      const result = await request(app)
        .get(`/api/activities?activityName=${activityData.name}`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.body.name, activityData.name);
      assert.strictEqual(result.body.instructor, activityData.instructor);
      assert.deepEqual(result.body.values, activityData.values);
    });
    it('Should list all activities names', async () => {
      const activities = Array.from(new Array(20)).map(() => dataBuilder.activity());
      await Promise.all(activities.map(a => activitiesRepo.insertOne(a)));
      const result = await request(app)
        .get('/api/activities?namesOnly=true')
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 200);
      assert.strictEqual(result.header['content-type'], 'application/json; charset=utf-8');
      assert.isTrue(Array.isArray(result.body));
      assert.strictEqual(result.body.length, 20);
      assert.sameMembers(result.body, activities.map(a => a.name));
    });
  });
  describe('Error cases', () => {
    it('Should answer with not found code when no activities names matchs', async () => {
      const activityData = dataBuilder.activity();
      await activitiesRepo.insertOne(activityData);
      const result = await request(app)
        .get(`/api/activities?activityName=foo${activityData.name}`)
        .set('Authorization', authHeader);
      assert.strictEqual(result.status, 404);
    });
    it('Should return conflit for attempts to duplicate names of activities on create', async () => {
      const activityData = dataBuilder.activity();
      const activityDataTwo = dataBuilder.activity({
        name: activityData.name
      });
      delete activityData._id;
      await request(app)
        .post('/api/activities')
        .set('Authorization', authHeader)
        .send(activityData);
      const result = await request(app)
        .post('/api/activities')
        .set('Authorization', authHeader)
        .send(activityDataTwo);
      assert.strictEqual(result.status, 409);
      const dbData = await activitiesRepo.findByQuery({}, {}, true);
      assert.strictEqual(dbData.length, 1);
    });
    it('Should return conflit for attempts to duplicate names of activities on update', async () => {
      const activityDataOne = dataBuilder.activity();
      const activityDataTwo = dataBuilder.activity();
      const { insertedId } = await activitiesRepo.insertOne(activityDataOne);
      await activitiesRepo.insertOne(activityDataTwo);
      delete activityDataOne._id;
      const previousOneName = activityDataOne.name;
      activityDataOne.name = activityDataTwo.name;
      const result = await request(app)
        .put(`/api/activities/${insertedId.toHexString()}`)
        .set('Authorization', authHeader)
        .send(activityDataOne);
      assert.strictEqual(result.status, 409);
      const activityOnDb = await activitiesRepo.findOneByQuery({
        _id: insertedId
      });
      delete activityOnDb._id;
      assert.strictEqual(activityOnDb.name, previousOneName);
    });
    it('Should answer with unauthorized code for requests without Authorization header', async () => {
      const result = await request(app).get(`/api/activities?activityName=foo`);
      assert.strictEqual(result.status, 401);
    });
    it('Should return unauthorized for requests with wrong data on authorization header', async () => {
      const result = await request(app)
        .get(`/api/activities?activityName=foo`)
        .set('Authorization', `${authHeader}asd`);
      assert.strictEqual(result.status, 401);
    });
  });
});
