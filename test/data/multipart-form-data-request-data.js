module.exports = {
  headers: {
    'content-type': 'multipart/form-data; boundary=AaB03x',
    'content-length': '16000'
  },
  body: Buffer.from('--AaB03x\r\n'
    + 'content-disposition: form-data; name="firstField"\r\n'
    + '\r\n'
    + 'firstFieldData\r\n'
    + '--AaB03x\r\n'
    + 'content-disposition: form-data; name="secondField"\r\n'
    + 'content-type: text/plain;charset=UTF-8\r\n'
    + '\r\n'
    + 'Dolores officia eligendi consequatur. Ea aut natus fugit consequatur non.´`\r\n'
    + '--AaB03x\r\n'
    + 'content-disposition: form-data; name="thirdField"\r\n'
    + '\r\n'
    + 'thirdFieldData\r\n'
    + '--AaB03x--\r\n')
};
