const { ObjectId } = require('../../lib/infra/db');
const buildRefMonth = require('../../lib/utils/build-reference-month');
const Chance = require('chance');
const chance = new Chance();
const refMonth = buildRefMonth(new Date());
const activitiesSchedule = (data = {}) => Object.assign({
  weekday: chance.natural({
    max: 6
  }),
  startHour: chance.natural({
    max: 23
  }),
  startMinutes: chance.natural({
    max: 59
  }),
  endHour: chance.natural({
    max: 23
  }),
  endMinutes: chance.natural({
    max: 59
  })
}, data);
const activity = (data = {}) => Object.assign({
  instructor: chance.name({
    middle: true
  }),
  name: chance.word({
    length: 20
  }),
  timesheet: [activitiesSchedule(), activitiesSchedule()],
  values: {
    single: chance.natural({
      max: 10
    }),
    monthly: chance.natural({
      max: 100
    })
  }
}, data);
const client = (clientData = {}) => Object.assign({
  birthDate: chance.pickone(['1986-10-21', '1986-10-22', '1986-09-21', '1986-11-21']),
  name: chance.name({
    middle: true
  }),
  activitiesData: [{
    id: new ObjectId(),
    customData: {
      belt: chance.color()
    }
  }, {
    id: new ObjectId()
  }],
  status: 'active'
}, clientData);
const debt = (debtData = {}) => Object.assign({
  activityName: chance.word(),
  clientId: new ObjectId(),
  details: {
    annotations: chance.paragraph(),
    dueDate: chance.date(),
    referenceMonth: refMonth
  },
  generated: {
    adminId: chance.word(),
    date: chance.date()
  },
  type: chance.pickone(['monthly payment', 'registration']),
  value: chance.natural({
    max: 100
  }),
  valid: true
}, debtData);
const payment = (data = {}) => Object.assign({
  adminId: chance.word(),
  date: chance.date(),
  debtsData: [{
    id: new ObjectId(),
    valueFactor: 1
  }],
  total: chance.natural({
    max: 1000
  })
}, data);
module.exports = {
  activitiesSchedule,
  activity,
  client,
  debt,
  payment
};