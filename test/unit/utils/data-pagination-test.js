const { assert } = require('chai');
const dataDriven = require('data-driven');
const dataPagination = require('../../../lib/utils/data-pagination');
describe('data pagination unit tests', () => {
  describe('Cases:', () => {
    dataDriven([{
      description: 'page: undefined, pagination: undefined',
      expectedResult: {
        limit: 10,
        skip: 0
      }
    }, {
      description: 'page: 1, pagination: 5',
      page: 1,
      pagination: 5,
      expectedResult: {
        limit: 5,
        skip: 0
      }
    }, {
      description: 'page: 5, pagination: 5',
      page: 5,
      pagination: 5,
      expectedResult: {
        limit: 5,
        skip: 20
      }
    }, {
      description: 'page: 1, pagination: 15 (should limit the result to 10 items)',
      page: 1,
      pagination: 15,
      expectedResult: {
        limit: 10,
        skip: 0
      }
    }, {
      description: 'page: 0, pagination: 9',
      page: 0,
      pagination: 9,
      expectedResult: {
        limit: 9,
        skip: 0
      }
    }, {
      description: 'page: -1, pagination: -7',
      page: -1,
      pagination: -7,
      expectedResult: {
        limit: 7,
        skip: 0
      }
    }], () => {
      it('{description}', (ctx) => {
        const result = dataPagination(ctx.pagination, ctx.page);
        assert.deepEqual(result, ctx.expectedResult);
      });
    });
  });
  it('Should handle the sort array', () => {
    const result = dataPagination(null, null, ['foo', 'bar']);
    assert.deepEqual(result.sort, {
      foo: 1,
      bar: 1
    });
  });
});
