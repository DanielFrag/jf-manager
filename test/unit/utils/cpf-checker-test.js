const { assert } = require('chai');
const dataDriven = require('data-driven');
const { validadeCpf } = require('../../../lib/utils/cpf-checker');
describe('cpf-checker unit test', () => {
  describe('Valid cpf', () => {
    dataDriven([{
      data: '347.496.221-33'
    }, {
      data: '219.706.502-57'
    }, {
      data: '675.255.777-22'
    }, {
      data: '927.354.772-90'
    }, {
      data: '036.483.245-27'
    }, {
      data: '29.771.548-84'
    }], () => {
      it('{data}', (ctx) => {
        assert.isTrue(validadeCpf(ctx.data));
      });
    });
    dataDriven([{
      data: '34749622133'
    }, {
      data: '21970650257'
    }, {
      data: '67525577722'
    }, {
      data: '92735477290'
    }, {
      data: '03648324527'
    }, {
      data: '2977154884'
    }], () => {
      it('{data}', (ctx) => {
        assert.isTrue(validadeCpf(ctx.data));
      });
    });
  });
  describe('Invalid cpf', () => {
    dataDriven([{
      data: '347.496.221-03'
    }, {
      data: '219.706.502-56'
    }, {
      data: '975.255.777-22'
    }, {
      data: '000.000.000-00'
    }, {
      data: '111.111.111-11'
    }], () => {
      it('{data}', (ctx) => {
        assert.isFalse(validadeCpf(ctx.data));
      });
    });
  });
});