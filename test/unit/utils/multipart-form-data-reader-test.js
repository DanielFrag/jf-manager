const dataDriven = require('data-driven');
const { headers, body } = require('../../data/multipart-form-data-request-data');
const RequestMock = require('../../mocks/request-mock');
const multipartFormDataReader = require('../../../lib/utils/multipart-form-data-reader');
const deliveryChunck = (requestMock, startIndex, endIndex) => {
  return new Promise((resolve) => {
    setImmediate(() => {
      requestMock.emit('data', requestMock.body.slice(startIndex, endIndex));
      resolve();
    });
  });
};
const scheduleChunks = (requestMock, indexesArray) => {
  let startIndex = 0;
  return indexesArray.reduce((t, c) => {
    return t.then(() => {
      const currentStart = startIndex;
      startIndex = c;
      return deliveryChunck(requestMock, currentStart, c);
    });
  }, Promise.resolve());
};
describe('multipart-form-data-reader unit tests', () => {
  const bodyLen = body.length;
  describe('Split body on index:', () => {
    dataDriven([{
      description: `5, 20 and ${bodyLen}`,
      indexArray: [5, 20, bodyLen]
    }, {
      description: 'all indexes',
      indexArray: Array.from(new Array(bodyLen)).map((c, i) => i + 1)
    }], () => {
      it('{description}', (ctx, done) => {
        const requestMock = new RequestMock(null, null, headers, body);
        scheduleChunks(requestMock, ctx.indexArray).then(() => {
          setImmediate(() => {
            requestMock.emit('end');
          });
        });
        multipartFormDataReader(requestMock, {}, (errorData) => {
          done(errorData);
        });
      });
    });
  });
});
