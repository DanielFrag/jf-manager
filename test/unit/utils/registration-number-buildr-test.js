const { assert } = require('chai');
const dataDriven = require('data-driven');
const registrationNumberBuilder = require('../../../lib/utils/registration-number-builder');
describe('registration-number-builder unit tests', () => {
  describe('Should use the default builder', () => {
    const currentYear = new Date().getFullYear();
    dataDriven([{
      input: 3,
      expected: parseInt(`${currentYear}0003`, 10)
    }, {
      input: 20,
      expected: parseInt(`${currentYear}0020`, 10)
    }, {
      input: 701,
      expected: parseInt(`${currentYear}0701`, 10)
    }, {
      input: 8403,
      expected: parseInt(`${currentYear}8403`, 10)
    }], () => {
      it('{input} -> {expected}', (ctx) => {
        const result = registrationNumberBuilder(ctx.input);
        assert.strictEqual(result, ctx.expected);
      });
    });
  });
  describe('Should use the builder plus1k', () => {
    dataDriven([{
      input: 3,
      expected: 1003
    }, {
      input: 20,
      expected: 1020
    }, {
      input: 701,
      expected: 1701
    }, {
      input: 8403,
      expected: 9403
    }, {
      input: 19978,
      expected: 20978
    }], () => {
      it('{input} -> {expected}', (ctx) => {
        const result = registrationNumberBuilder(ctx.input, 'plus1k');
        assert.strictEqual(result, ctx.expected);
      });
    });
  });
});
