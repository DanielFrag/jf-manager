const { assert } = require('chai');
const dataDriven = require('data-driven');
const fs = require('fs');
const path = require('path');
const validation = require('../../../lib/validation');
describe('Schema validation unit tests', () => {
  before(async () => {
    await validation.loadSchemas();
  });
  describe('Valid objects', () => {
    const dataDrivenArray = [{
      object: {
        birthDate: '1986-06-10',
        cpf: '11122233344',
        name: 'Sunda Foo Bar',
        phone: '21988887777',
        photo: null,
        registrationNumber: 8888,
        status:'active',
        debtsExpireDate: 1,
        activitiesData: [{
          name: 'bobra - noite',
          customData: {
            belt: 'white'
          }
        }, {
          name: 'bobra Do - manhã',
          customData: {
            weight: 89,
            height: 1.78
          }
        }, {
          name: 'Foo Do - manhã'
        }]
      },
      schemaName: 'client'
    }, {
      object: {
        cpf: '11122233344',
        debtsExpireDate: 1,
        phone: '21988887777',
        status:'active'
      },
      schemaName: 'patchClient'
    }, {
      object: {
        name: 'muay-tay',
        instructor: 'Sunda',
        timesheet: [{
          weekday: 2,
          endHour: 15,
          endMinutes: 30,
          startHour: 14,
          startMinutes: 0
        }, {
          weekday: 4,
          endHour: 15,
          endMinutes: 30,
          startHour: 14,
          startMinutes: 0
        }],
        values: {
          single: 5,
          monthly: 50
        }
      },
      schemaName: 'activity'
    }, {
      object: {
        clientId: '2cdfb11e1f3c000000007803',
        details: {
          referenceMonth: '2019-02',
          dueDate: '2019-01-02',
          annotation: 'lorem ipsum'
        },
        generated: {
          date: '2019-02-01T03:45:36.000Z',
          adminId: '2cdfb11e1f3c000000007805'
        },
        paymentId: '4cdfb11e1f3c000000007822',
        activityName: 'lorem ipsum',
        type: 'monthly payment',
        value: 19.9,
        valid: true
      },
      schemaName: 'debt'
    }, {
      object: {
        adminId: '1cdfb11e1f3c000000007915',
        date: '2019-02-01T03:45:36.000Z',
        debtsData: [{
          id: '4cdfb11e1f3c000000007903',
          valueFactor: 1.2
        }, {
          id: '4cdfb11e1f3c000000007901',
          valueFactor: 0.8
        }],
        total: 50
      },
      schemaName: 'payment'
    }, {
      object: [{
        id: '4cdfb11e1f3c000000007903',
        valueFactor: 1.2
      }, {
        id: '4cdfb11e1f3c000000007901'
      }],
      schemaName: 'postPayment'
    }];
    before((done) => {
      const imagePath = path.resolve(__dirname, '../../data/image-example.png');
      fs.readFile(imagePath, 'ascii', (readFileError, image) => {
        dataDrivenArray[0].object.photo = image;
        done(readFileError);
      });
    });
    dataDriven(dataDrivenArray, () => {
      it('{schemaName}', (ctx) => {
        const result = validation.validateSchema(ctx.object, ctx.schemaName);
        assert.isTrue(result.valid);
      });
    });
  });
  describe('Invalid objects', () => {
    const dataDrivenArray = [{
      object: {
        birthDate: '1986-06-10T01:45:43.000Z',
        cpf: '1112223334',
        name: 'S',
        phone: '218888777',
        photo: Buffer.from('sunda'),
        registrationNumber: -1,
        status:'foo',
        debtsExpireDate: '10',
        activitiesData: [{
          name: 123
        }]
      },
      pathsToCheck: [
        '/properties/birthDate/format',
        '/properties/cpf/format',
        '/properties/name/minLength',
        '/properties/phone/minLength',
        '/properties/registrationNumber/minimum',
        '/properties/debtsExpireDate/type',
        '/properties/status/type',
        '/properties/activitiesData/items/properties/name/type',
        '/properties/photo/type'
      ],
      schemaName: 'client'
    }, {
      object: {
        cpf: '111222333444',
        phone: 2188887777,
        status: true,
        debtsExpireDate: 29,
        activitiesData: {}
      },
      pathsToCheck: [
        '/required/0',
        '/required/1',
        '/properties/cpf/format',
        '/properties/phone/type',
        '/properties/status/type',
        '/properties/debtsExpireDate/maximum',
        '/properties/activitiesData/type'
      ],
      schemaName: 'client'
    }, {
      object: {
        debtsExpireDate: 0
      },
      pathsToCheck: [
        '/required/0',
        '/required/1',
        '/properties/debtsExpireDate/minimum'
      ],
      schemaName: 'client'
    }, {
      object: {
        cpf: '111222',
        debtsExpireDate: 30,
        phone: '988887777',
        status: 'sunda'
      },
      pathsToCheck: [
        '/properties/cpf/format',
        '/properties/debtsExpireDate/maximum',
        '/properties/phone/minLength',
        '/properties/status/type'
      ],
      schemaName: 'patchClient'
    }, {
      object: {
        debtsExpireDate: 0
      },
      pathsToCheck: ['/properties/debtsExpireDate/minimum'],
      schemaName: 'patchClient'
    }, {
      object: {
        name: 'F',
        instructor: 2,
        timesheet: {}
      },
      pathsToCheck: [
        '/properties/name/minLength',
        '/properties/instructor/type',
        '/properties/timesheet/type'
      ],
      schemaName: 'activity'
    }, {
      object: {
        name: 'Foo',
        instructor: 'Bar',
        timesheet: [{
          foo: 9
        }, {
          weekday: 7,
          endHour: 25,
          endMinutes: -1,
          startHour: 'bar',
          startMinutes: 60
        }]
      },
      pathsToCheck: [
        '/properties/timesheet/items/required/0',
        '/properties/timesheet/items/required/1',
        '/properties/timesheet/items/required/2',
        '/properties/timesheet/items/required/3',
        '/properties/timesheet/items/required/4',
        '/properties/timesheet/items/properties/weekday/maximum',
        '/properties/timesheet/items/properties/endHour/maximum',
        '/properties/timesheet/items/properties/endMinutes/minimum',
        '/properties/timesheet/items/properties/startHour/type',
        '/properties/timesheet/items/properties/startMinutes/maximum'
      ],
      schemaName: 'activity'
    }, {
      object: {
        details: {
          referenceMonth: '1986-06-10T01:45:43.000Z',
          dueDate: '01:45:43.000Z',
          annotations: 90
        },
        generated: {
          adminId: 12,
          date: '1986-06-10'
        },
        paymentId: '4cdfb11e1f3c0000000078223'
      },
      pathsToCheck: [
        '/required/0',
        '/required/2',
        '/required/3',
        '/required/4',
        '/properties/details/properties/referenceMonth/format',
        '/properties/details/properties/dueDate/format',
        '/properties/details/properties/annotations/type',
        '/properties/generated/properties/adminId/type',
        '/properties/generated/properties/date/format',
        '/properties/paymentId/maxLength'
      ],
      schemaName: 'debt'
    }, {
      object: {
        debtsData: [{
          id: 123,
          valueFactor: '1.2'
        }, {
          id: '4cdfb11e1f3c00000000790'
        }]
      },
      pathsToCheck: [
        '/required/0',
        '/required/1',
        '/required/3',
        '/properties/debtsData/items/properties/id/type',
        '/properties/debtsData/items/properties/valueFactor/type',
        '/properties/debtsData/items/required/1',
        '/properties/debtsData/items/properties/id/minLength'
      ],
      schemaName: 'payment'
    }, {
      object: [{
        valueFactor: 1.2
      }, {
        id: '4cdfb11e1f3c000000007901'
      }],
      pathsToCheck: [
        '/items/required/0'
      ],
      schemaName: 'postPayment'
    }];
    dataDriven(dataDrivenArray, () => {
      it('{schemaName}', (ctx) => {
        const result = validation.validateSchema(ctx.object, ctx.schemaName);
        assert.isFalse(result.valid);
        assert.isOk(result.errors);
        assert.strictEqual(result.errors.length, ctx.pathsToCheck.length);
        const errorsPath = result.errors.map(e => e.path);
        assert.sameMembers(errorsPath, ctx.pathsToCheck, JSON.stringify(result));
      });
    });
    it('Should check invalid objects with validation in strict mode', () => {
      const data = {
        name: 'muay-tay',
        instructor: 'Sunda',
        timesheet: [{
          weekday: 2,
          endHour: 15,
          endMinutes: 30,
          startHour: 14,
          startMinutes: 0,
          startSeconds: 1
        }, {
          weekday: 4,
          endHour: 15,
          endMinutes: 30,
          startHour: 14,
          startMinutes: 0
        }],
        values: {
          single: 5,
          monthly: 50
        }
      };
      const result = validation.validateSchema(data, 'activity', true);
      assert.isFalse(result.valid);
      assert.strictEqual(result.errors.length, 1);
      assert.strictEqual(result.errors[0].message, 'Unknown property (not in schema)');
    });
  });
});
