const db = require('../lib/infra/db');
module.exports = {
  dropCollectionsHelper(collectionsArray) {
    return collectionsArray
      .reduce((t, c) => t
        .then(() => db.getCollection(c).drop())
        .catch(() => db.getCollection(c).drop()), Promise.resolve())
      .catch(() => Promise.resolve());
  }
};
