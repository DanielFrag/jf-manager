const validation = require('../lib/validation');
const db = require('../lib/infra/db');
const applicationConfig = require('../lib/config/application-config');
before(async () => {
  await validation.loadSchemas();
  await db.connect(`${applicationConfig.MONGO_URI}_test`);
});
after(async () => {
  await db.close();
});
