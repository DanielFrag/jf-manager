const { EventEmitter } = require('events');
function RequestMock(params, query, headers, body) {
  this.query = query;
  this.params = params;
  this.headers = headers;
  this.body = body;
}
RequestMock.prototype = Object.create(EventEmitter.prototype);
module.exports = RequestMock;
