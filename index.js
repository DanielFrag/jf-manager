const application = require('./lib/application');
const shutdown = async () => {
  console.log('graceful shutdown in progress');
  await application.closeApplication();
  process.exit(0);
};
process
  .on('SIGINT', shutdown)
  .on('SIGTERM', shutdown)
  .on('uncaughtException', (errorData) => {
    console.log(errorData);
    process.exit(1);
  });
(async () => {
  await application.startApplication();
  await application.startListen();
})();
