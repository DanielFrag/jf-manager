const express = require('express');
const bodyParser = require('body-parser');
const routerConfig = require('./router-config');
const app = express();
app.set('port', process.env.PORT || 8080);
app.use(bodyParser.json({
  type: 'application/json'
}));
routerConfig(app);
module.exports = app;
