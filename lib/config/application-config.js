module.exports = {
  DAYS_TO_EXPIRE_DEBTS: process.env.DAYS_TO_EXPIRE_DEBTS || 5,
  DEFAULT_EXPIRE_DATE: process.env.DEFAULT_EXPIRE_DATE || 5,
  MONGO_URI: process.env.MONGODB_URI || 'mongodb://localhost:27017/jf-manager',
  REGISTRATION_VALUE: process.env.REGISTRATION_VALUE || 25,
  SECRET: process.env.SECRET || 'secret',
  USER_NAME: process.env.USER_NAME || 'FoO',
  USER_PASS: process.env.USER_PASS || 'bAr'
};
