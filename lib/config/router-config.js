const { Router } = require('express');
const activitiesRouter = require('../routes/activities-routes');
const clientRouter = require('../routes/clients-routes');
const debtsRouter = require('../routes/debts-routes');
const paymentsRouter = require('../routes/payments-routes');
const tasksRouter = require('../routes/tasks-routers');
const authMiddleware = require('../middleware/auth');
const router = Router();
module.exports = (app) => {
  router.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PATCH,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');
    if (req.method === 'OPTIONS') {
      return res.sendStatus(200);
    }
    next();
  });
  router.use(authMiddleware);
  router.use('/activities', activitiesRouter);
  router.use('/clients', clientRouter);
  router.use('/debts', debtsRouter);
  router.use('/payments', paymentsRouter);
  router.use('/tasks', tasksRouter);
  app.use('/api', router);
};
