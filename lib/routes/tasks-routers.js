const { Router } = require('express');
const tasksController = require('../controllers/tasks-controller');
const router = Router();
router.post('/generate-monthly-debts', tasksController.generateMonthlyDebts);
module.exports = router;
