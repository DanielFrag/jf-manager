const { Router } = require('express');
const debtsControllers = require('../controllers/debts-controller');
const router = Router();
router.post('/', debtsControllers.validatePostDebt, debtsControllers.postDebt);
router.get('/:debtId', debtsControllers.getDebtById);
module.exports = router;
