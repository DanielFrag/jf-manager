const { Router } = require('express');
const paymentsControllers = require('../controllers/payments-controller');
const router = Router();
router
  .route('/')
  .post(paymentsControllers.validatePostPayment, paymentsControllers.postPayment)
  .get(paymentsControllers.getPaymentsReportsList);
module.exports = router;
