const { Router } = require('express');
const clientControllers = require('../controllers/clients-controller');
const router = Router();
router
  .route('/')
  .get(clientControllers.getClientsByQuery)
  .post(clientControllers.validateCreateClientData, clientControllers.postClient);
router.patch('/:clientId/image', clientControllers.patchImageMiddleware ,clientControllers.patchImage);
router
  .route('/:clientId')
  .get(clientControllers.getClientById)
  .patch(clientControllers.validateUpdateClientData, clientControllers.patchClient);
router.patch('/:clientId/activities',
  clientControllers.validatePatchActivity, clientControllers.pushClientActivity);
router.delete('/:clientId/activities/:activityName', clientControllers.pullClientActivity);
module.exports = router;
