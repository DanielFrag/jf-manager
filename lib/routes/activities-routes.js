const { Router } = require('express');
const activitiesController = require('../controllers/activities-controller');
const router = Router();
router
  .route('/')
  .get(activitiesController.listActivities)
  .post(activitiesController.validateActivity, activitiesController.postActivity);
router
  .route('/:activityId')
  .delete(activitiesController.deleteActivity)
  .get(activitiesController.getActivity)
  .put(activitiesController.validateActivity, activitiesController.putActivity);
module.exports = router;
