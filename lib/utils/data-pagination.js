module.exports = (pagination, page, sort) => {
  const paginationNumber = Math.abs(pagination);
  const limit = paginationNumber && paginationNumber < 11 ? paginationNumber : 10;
  const pageNumber = Math.abs(page);
  const skip = ((pageNumber && pageNumber > 0 ? pageNumber : 1) - 1) * limit;
  const result = {
    limit,
    skip
  };
  if (Array.isArray(sort)) {
    result.sort = {};
    sort.forEach((field) => {
      result.sort[field] = 1;
    });
  } else if (typeof sort === 'string') {
    result.sort = {};
    result.sort[sort] = 1;
  }
  return result;
};
