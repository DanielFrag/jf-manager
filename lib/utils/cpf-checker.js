const checkDigit = (cpfBlock, digit) => {
  let dResult = cpfAcc(cpfBlock) % 11;
  if (dResult < 2) {
    dResult = 0;
  } else {
    dResult = 11 - dResult;
  }
  return dResult == digit;
};
const cpfAcc = (cpfSubStr) => {
  let len = cpfSubStr.length, acc = 0;
  for (let i = len - 1; i >=0; i--) {
    acc += parseInt(cpfSubStr[i]) * (1 + (len - i));
  }
  return acc;
};
const validadeCpf = stringCpf => !stringCpf.split('').reduce((t, c) => t === c ? c : false)
    && checkDigit(stringCpf.substring(0, 9), parseInt(stringCpf.charAt(9)))
    && checkDigit(stringCpf.substring(0, 10), parseInt(stringCpf.charAt(10)));
const formatCpf = (cpf) => {
  let cpfNumbers = cpf.replace(/\.|-/gi, '');
  const len = cpfNumbers.length;
  if (len < 11) {
    return new Array(11 - cpfNumbers.length + 1).join('0') + cpfNumbers;
  }
  return cpfNumbers;
};
module.exports = {
  formatCpf,
  validadeCpf(cpf) {
    return validadeCpf(formatCpf(cpf));
  }
};
