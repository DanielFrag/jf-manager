const { EventEmitter } = require('events');
const crlfAsBuffer = Buffer.from('\r\n');
const doubleCrlfAsBuffer = Buffer.from('\r\n\r\n');
function customSlplit(bufferDelimiter) {
  const thisLen = this.length;
  const delimiterLen = bufferDelimiter.length;
  const parts = [];
  let delimiterIndex = 0;
  let lastScannedIndex = 0;
  do {
    const startIndex = lastScannedIndex;
    delimiterIndex = this.indexOf(bufferDelimiter, startIndex);
    if (delimiterIndex === -1) {
      delimiterIndex = lastScannedIndex = this.length;
    } else {
      lastScannedIndex = delimiterIndex + delimiterLen;
    }
    parts.push(this.slice(startIndex, delimiterIndex));
  } while (lastScannedIndex !== thisLen);
  return parts;
}
function MultipartFormDataReader(req, options = {}) {
  const [contentType, ...contentTypeData] = req.headers['content-type'].split('; ');
  if (contentType !== 'multipart/form-data') {
    throw new Error('The content-type should be multipart/form-data');
  }
  const boundaryData = contentTypeData.find(d => d.indexOf('boundary=') !== -1);
  const [, boundaryMark] = boundaryData.split('boundary=');
  this.formBoundaryBuffer = Buffer.from(`--${boundaryMark}`);
  this.req = req;
  this.options = options;
  this.unhandledChunk = Buffer.from([]);
  this.currentChunk;
  this.unhandledPart;
  this.currentPartHeader;
  this.currentPartData;
  this.dataDictionary = new Map();
}
MultipartFormDataReader.prototype = Object.create(EventEmitter.prototype, MultipartFormDataReader.prototype);
MultipartFormDataReader.prototype.extractPartData = function(part, startIndex) {
  const dataStartIndex = part.indexOf(doubleCrlfAsBuffer, startIndex);
  return part.slice(dataStartIndex + doubleCrlfAsBuffer.length, part.length);
};
MultipartFormDataReader.prototype.tryExtractPartHeader = function(part) {
  let firstIndex;
  const hasStartIndex = ['Content-Disposition', 'content-disposition'].some(item => {
    firstIndex = part.indexOf(Buffer.from(item));
    return firstIndex !== -1;
  });
  if (!hasStartIndex) {
    return;
  }
  const lastIndex = part.indexOf(doubleCrlfAsBuffer, firstIndex);
  const headerText = Buffer.from(part.slice(firstIndex, lastIndex)).toString('ascii');
  const nameFirstIndex = headerText.indexOf('name="') + 6;
  const nameLastIndex = headerText.indexOf('"', nameFirstIndex);
  const name = headerText.substring(nameFirstIndex, nameLastIndex);
  return {
    name,
    text: headerText
  };
};
MultipartFormDataReader.prototype.deliveryPart = function(part) {
  if (this.unhandledPart && this.unhandledPart.length) {
    part = Buffer.concat([this.unhandledPart, part]);
    this.unhandledPart = null;
  }
  if (this.currentPartHeader) {
    this.currentPartData = Buffer.concat([this.currentPartData, part]);
    return;
  }
  this.currentPartHeader = this.tryExtractPartHeader(part);
  if (!this.currentPartHeader) {
    this.unhandledPart = part;
    return;
  }
  this.currentPartData = this.extractPartData(part, this.currentPartHeader.text.length);
};
MultipartFormDataReader.prototype.finishPreviousPart = function() {
  const dataLastNewLineIndex = this.currentPartData.lastIndexOf(crlfAsBuffer);
  if (dataLastNewLineIndex === this.currentPartData.length - crlfAsBuffer.length) {
    this.currentPartData = this.currentPartData.slice(0, dataLastNewLineIndex);
  }
  this.dataDictionary.set(this.currentPartHeader.name, {
    header: this.currentPartHeader,
    data: this.currentPartData
  });
  this.currentPartHeader = null;
  this.currentPartData = null;
};
MultipartFormDataReader.prototype.handleDeliveryPart = function(part) {
  const partLen = part.length;
  const untilIndex = 1 + partLen - this.formBoundaryBuffer.length;
  this.deliveryPart(part.slice(0, untilIndex));
  this.unhandledChunk = part.slice(untilIndex, partLen);
};
MultipartFormDataReader.prototype.handleParts = function(parts) {
  if (parts.length === 1) {
    return this.handleDeliveryPart(parts[0]);
  }
  const lastPart = parts.pop();
  this.unhandledChunk = lastPart;
  parts.forEach((p) => {
    this.deliveryPart(p);
    if (this.currentPartHeader) {
      this.finishPreviousPart();
    }
  });
};
MultipartFormDataReader.prototype.handleChunck = function() {
  let chunckBuffer = Buffer.concat([this.unhandledChunk, this.currentChunk]);
  if (chunckBuffer.length < this.formBoundaryBuffer.length) {
    this.unhandledChunk = chunckBuffer;
    return;
  }
  const delimiter = this.formBoundaryBuffer;
  const parts = customSlplit.apply(chunckBuffer, [delimiter]);
  this.handleParts(parts);
};
MultipartFormDataReader.prototype.read = function(callback) {
  this.req
    .on('data', (chunk) => {
      this.currentChunk = Buffer.from(chunk);
      this.handleChunck();
    })
    .on('error', errorData => callback(errorData))
    .on('end', () => callback(null, this.dataDictionary));
};
module.exports = (req, options, callback) => {
  const hasCallback = callback && typeof callback === 'function';
  const readerPromise = new Promise((resolve, reject) => {
    const endCallback = hasCallback ? (errorData, result) => {
      callback(errorData, result);
      resolve(result);
    } : (errorData, result) => errorData ? reject(errorData) : resolve(result);
    setImmediate(() => {
      const reqReader = new MultipartFormDataReader(req, options);
      reqReader.read(endCallback);
    });
  });
  if (hasCallback) {
    readerPromise.catch(callback);
  }
  return readerPromise;
};
