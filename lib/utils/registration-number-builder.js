const builders = {
  default(sourceInteger) {
    const strInteger = sourceInteger.toString();
    return parseInt(`${new Date().getFullYear()}${strInteger.padStart(4, '0')}`, 10);
  },
  plus1k(sourceInteger) {
    return sourceInteger + 1000;
  }
};
module.exports = (sourceInteger, builderType) => {
  if (typeof sourceInteger !== 'number' || sourceInteger % 1 !== 0) {
    return {
      error: new Error('The sourceInteger must has the type number and be an integer')
    };
  }
  if (!builderType) {
    return builders.default(sourceInteger);
  }
  if (!builders[builderType]) {
    return {
      error: new Error('Could not found the requested builder')
    };
  }
  return builders[builderType](sourceInteger);
};
