function KeyLocker() {
  this._set = new Set();
}
KeyLocker.prototype.lock = function(key) {
  if (this._set.has(key)) {
    return Promise.reject('The key is already locked');
  }
  this._set.add(key);
  return Promise.resolve();
};
KeyLocker.prototype.unlock = function(key) {
  if (!this._set.has(key)) {
    return Promise.reject('The key is not locked');
  }
  this._set.delete(key);
  return Promise.resolve();
};
module.exports = () => new KeyLocker();
