module.exports = date => `${date.getUTCFullYear()}-${(1 + date.getUTCMonth()).toString().padStart(2, '0')}`;
