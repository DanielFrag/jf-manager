const sharp = require('sharp');
const resizeImage = (bufferImage, width, height) => sharp(bufferImage)
  .resize(width, height, {
    fit: 'inside'
  })
  .toBuffer();
module.exports = {
  resizeImage
};
