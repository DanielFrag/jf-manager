const validationMiddleware = require('./validation-middleware');
const debtsService = require('../services/debt-service');
const paymentsService = require('../services/payments-service');
const postPayment = async (req, res) => {
  const [...debtsData] = req.body;
  const factorDictionary = new Map();
  const debtsIdsSet = new Set();
  debtsData.forEach(dData => {
    debtsIdsSet.add(dData.id);
    if (typeof dData.valueFactor === 'number') {
      factorDictionary.set(dData.id, dData.valueFactor);
    }
  });
  const dbDebts = await debtsService.getDebtsByIds(debtsIdsSet, true);
  if (debtsIdsSet.size !== dbDebts.length) {
    return res.status(412).send('Could not found all the requested debts');
  }
  if (dbDebts.some(d => d.paymentId)) {
    return res.status(412).send('Some debt was already paid');
  }
  const paymentId = await paymentsService.createPayment('post', dbDebts, factorDictionary);
  await debtsService.setPaymentToDebts(debtsIdsSet, paymentId);
  res.set('Location', `/api/payments/${paymentId}`);
  res.sendStatus(201);
};
const validatePostPayment = (req, res, next) => validationMiddleware('postPayment',
  req.body, true, res, next);
const getPaymentsReportsList = async (req, res) => {
  const { startDate, page, pagination } = req.query;
  const list = await paymentsService.getPaymentEnrichedList(new Date(startDate), page, pagination);
  res.json(list);
};
module.exports = {
  getPaymentsReportsList,
  postPayment,
  validatePostPayment
};
