const validationMiddleware = require('./validation-middleware');
const activitiesService = require('../services/activities-service');
const validateActivity = (req, res, next) => validationMiddleware('activity', req.body, true, res, next);
const postActivity = async (req, res) => {
  const { name: activityName } = req.body;
  const previousActivity = await activitiesService.getActivityByName(activityName);
  if (previousActivity) {
    return res.status(409).send('The activity name already exists');
  }
  const activityId = await activitiesService.createActivity(req.body);
  res.set('Location', `/api/activities/${activityId}`);
  res.sendStatus(201);
};
const deleteActivity = async (req, res) => {
  const { activityId } = req.params;
  const deleteResult = await activitiesService.deleteActivity(activityId);
  if (deleteResult) {
    return res.status(deleteResult.status).send(deleteResult.error.message);
  }
  res.sendStatus(204);
};
const getActivity = async (req, res) => {
  const { activityId } = req.params;
  const activity = await activitiesService.getActivityById(activityId);
  if (!activity) {
    return res.status(404).send('Could not found the requested activity');
  }
  res.json(activity);
};
const listActivities = async (req, res) => {
  const { page, pagination, activityName, namesOnly } = req.query;
  if (activityName) {
    const activity = await activitiesService.getActivityByName(activityName);
    if (!activity) {
      return res.sendStatus(404);
    }
    return res.json(activity);
  }
  if (namesOnly && namesOnly === 'true') {
    const names = await activitiesService.getActivitiesNames();
    return res.json(names);
  }
  const activitiesList = await activitiesService.getActivities(pagination, page);
  res.json(activitiesList);
};
const putActivity = async (req, res) => {
  const { activityId } = req.params;
  const { name } = req.body;
  const previousActivity = await activitiesService.getActivityByName(name);
  if (previousActivity && previousActivity._id !== activityId) {
    return res.status(409).send('This activity name is related to another activity');
  }
  const result = await activitiesService.updateActivityById(activityId, req.body);
  if (result) {
    return res.status(result.statusCode).send(result.error.message);
  }
  res.sendStatus(204);
};
module.exports = {
  deleteActivity,
  getActivity,
  listActivities,
  postActivity,
  putActivity,
  validateActivity
};
