const validation = require('../validation');
module.exports = (schemaId, body, strictMode, res, next) => {
  const isValidClientData = validation.validateSchema(body, schemaId, strictMode);
  if (isValidClientData.valid) {
    return next();
  }
  return res.status(400).send(isValidClientData.errors);
};