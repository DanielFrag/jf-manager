const {
  REGISTRATION_VALUE: registrationValue,
  DAYS_TO_EXPIRE_DEBTS: daysToExpireDebts
} = require('../config/application-config');
const validationMiddleware = require('./validation-middleware');
const multipartFormReader = require('../utils/multipart-form-data-reader');
const registrationNumberBuilder = require('../utils/registration-number-builder');
const clientService = require('../services/clients-service');
const debtService = require('../services/debt-service');
const cpfChecker = require('../utils/cpf-checker');
const imageProcessor = require('../utils/image-processor');
const findClientById = async (clientId) => {
  const client = await clientService.getClientById(clientId);
  return client;
};
const getClientById = async (req, res) => {
  const { clientId } = req.params;
  const client = await clientService.getClientById(clientId);
  if (!client) {
    return res.sendStatus(404);
  }
  res.json(client);
};
const getClientsByQuery = async (req, res) => {
  const { name, registrationNumber, page, pagination, sort } = req.query;
  const pageOpt = parseInt(page, 10);
  const paginationOpt = parseInt(pagination, 10);
  if (!registrationNumber && !name) {
    const clients = await clientService.getClients(pageOpt, paginationOpt, sort);
    return res.json(clients);
  }
  const clientData = {};
  if (registrationNumber) {
    clientData.registrationNumber = parseInt(registrationNumber, 10);
  }
  if (name) {
    clientData.name = new RegExp(name.toUpperCase());
  }
  if (!clientData.registrationNumber && !clientData.name) {
    return res.status(400).send('Invalid registration number');
  }
  const clients = await clientService.getClientsFullData(clientData, pageOpt, paginationOpt, sort);
  if (!clients) {
    return res.sendStatus(404);
  }
  return res.json(clients);
};
const postClient = async (req, res) => {
  const {
    birthDate,
    cpf,
    debtsExpireDate,
    name,
    phone
  } = req.body;
  const validBirthDate = new Date(birthDate);
  if (validBirthDate > new Date()) {
    return res.status(400).send('The birth date is invalid');
  }
  const validName = name.toUpperCase();
  const previousClient = await clientService.getClientByNameAndBirthDate(validName, validBirthDate);
  if (previousClient) {
    return res.status(409).send('The client already exists');
  }
  const formatedCpf = cpf && cpfChecker.formatCpf(cpf);
  if (formatedCpf && !cpfChecker.validadeCpf(formatedCpf)) {
    return res.status(400).send('The cpf is invalid');
  }
  const clientId = await clientService.createClient(validName, validBirthDate, phone, formatedCpf, debtsExpireDate);
  const clientPosition = await clientService.getClientPositionForRegistrationNumber();
  await clientService.updateClient(clientId, {
    registrationNumber: registrationNumberBuilder(clientPosition + 1),
    status: 'active'
  });
  await debtService.generateRegistrationDebt(clientId, registrationValue, daysToExpireDebts);
  res.set('Location', `/api/clients/${clientId}`);
  res.sendStatus(201);
};
const patchImageMiddleware = (req, res, next) => {
  const contentLength = parseInt(req.headers['content-length'], 10);
  if (contentLength > 2000000) {
    req.destroy();
    return res.status(400).send('The body is to large');
  }
  next();
};
const patchImage = async (req, res) => {
  const { clientId } = req.params;
  const clientExists = !!await findClientById(clientId);
  if (!clientExists) {
    return res.sendStatus(404);
  }
  const dataDictionary = await multipartFormReader(req);
  const { data } = dataDictionary.get('photo');
  const resizedImage = await imageProcessor.resizeImage(data, 133, 150);
  await clientService.patchClientPhoto(clientId, resizedImage.toString('base64'));
  res.sendStatus(204);
};
const validateCreateClientData = (req, res, next) => validationMiddleware('client',
  req.body, false, res, next);
const validateUpdateClientData = (req, res, next) => validationMiddleware('patchClient',
  req.body, true, res, next);
const validatePatchActivity = (req, res, next) => validationMiddleware('clientActivity',
  req.body, false, res, next);
const patchClient = async (req, res) => {
  const { clientId } = req.params;
  const clientExists = await findClientById(clientId);
  if (!clientExists) {
    return res.sendStatus(404);
  }
  const updateData = {};
  const {
    cpf,
    debtsExpireDate,
    phone,
    status
  } = req.body;
  if (cpf) {
    const formatedCpf = cpf && cpfChecker.formatCpf(cpf);
    if (formatedCpf && !cpfChecker.validadeCpf(formatedCpf)) {
      return res.status(400).send('The cpf is invalid');
    }
    updateData.cpf = formatedCpf;
  }
  if (debtsExpireDate) {
    updateData.debtsExpireDate = debtsExpireDate;
  }
  if (status && clientExists.status !== 'inactive' && status !== clientExists.status) {
    updateData.status = status;
  }
  if (phone) {
    updateData.phone = phone;
  }
  if (!Object.keys(updateData).length) {
    return res.sendStatus(204);
  }
  await clientService.updateClient(clientId, updateData);
  res.sendStatus(204);
};
const pullClientActivity = async (req, res) => {
  const { clientId, activityName } = req.params;
  const resultError = await clientService.pullClientActivity(clientId, activityName);
  if (resultError) {
    return res.status(resultError.statusCode).send(resultError.message);
  }
  res.sendStatus(204);
};
const pushClientActivity = async (req, res) => {
  const { clientId } = req.params;
  const client = await findClientById(clientId);
  if (!client) {
    return res.sendStatus(404);
  }
  const { name: activityName, customData } = req.body;
  const activityAlreadyPushed = client.activitiesData.some(a => a.name === activityName);
  if (activityAlreadyPushed) {
    await clientService.updateClientActivityCustomData(clientId, activityName, customData);
    return res.sendStatus(204);
  }
  const result = await clientService.pushClientActivity(clientId, {
    name: activityName,
    customData
  });
  if (result && result.error) {
    return res.status(result.statusCode).send(result.error.message);
  }
  res.sendStatus(204);
};
module.exports = {
  getClientById,
  getClientsByQuery,
  postClient,
  patchImage,
  patchImageMiddleware,
  patchClient,
  pullClientActivity,
  pushClientActivity,
  validateCreateClientData,
  validatePatchActivity,
  validateUpdateClientData
};
