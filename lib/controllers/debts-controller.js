const activitiesService = require('../services/activities-service');
const debtsService = require('../services/debt-service');
const clientsService = require('../services/clients-service');
const validationMiddleware = require('./validation-middleware');
const validatePostDebt = (req, res, next) => validationMiddleware('postDebt', req.body, true, res, next);
const createMonthlyDebt = async (activityName, clientId, dueDate, referenceMonth) => {
  const client = await clientsService.getClientById(clientId);
  if (!client) {
    return {
      error: 'The client not exists'
    };
  }
  const activity = await activitiesService.getActivityByName(activityName);
  if (!activity) {
    return {
      error: 'Could not found the activity data'
    };
  }
  const conflit = await debtsService.findClientDebtByActivityAndRefMonth(clientId, activityName,
    referenceMonth);
  if (conflit) {
    return {
      conflit: 'The debt was already created'
    };
  }
  if (!client.activitiesData.some(aData => aData.name === activity.name)) {
    return {
      error: 'The activity is not associated to client'
    };
  }
  const insertedId = await debtsService.generateMonthlyDebt(clientId, activity, dueDate, referenceMonth);
  return insertedId;
};
const getDebtById = async (req, res) => {
  const { debtId } = req.params;
  const debt = await debtsService.getDebtById(debtId);
  if (!debt) {
    return res.sendStatus(404);
  }
  return res.json(debt);
};
const postDebt = async (req, res) => {
  const { activityName, clientId, dueDate, referenceMonth } = req.body;
  let usedDueDate;
  if (dueDate) {
    usedDueDate = new Date(dueDate);
  } else {
    usedDueDate = new Date(`${referenceMonth}-01`);
    usedDueDate.setDate(usedDueDate.getDate() + new Date().getDate() + 1);
  }
  const result = await createMonthlyDebt(activityName, clientId, usedDueDate, referenceMonth);
  if (result.conflit) {
    return res.status(409).send(result.conflit);
  }
  if (result.error) {
    return res.status(412).send(result.error);
  }
  res.set('Location', `/api/debts/${result}`);
  res.sendStatus(201);
};
module.exports = {
  getDebtById,
  postDebt,
  validatePostDebt
};
