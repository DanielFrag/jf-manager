const tasksService = require('../services/tasks-service');
const { validateFormat } = require('../validation/index');
const generateMonthlyDebts = (req, res) => {
  const { referenceMonth } = req.body;
  if (!referenceMonth || !validateFormat(referenceMonth, 'year-month').valid) {
    return res.sendStatus(400);
  }
  tasksService.generateMonthlyDebts(referenceMonth);
  res.sendStatus(202);
};
module.exports = {
  generateMonthlyDebts
};
