const { ObjectId } = require('../infra/db');
const dataPagination = require('../utils/data-pagination');
const paymentsRepository = require('../repositories/payments-repository');
const createPayment = async (adminId, debtsArray, factorDictionary) => {
  const debtsData = [];
  let total = 0;
  debtsArray.forEach((debt) => {
    const factor = factorDictionary.get(debt._id) || 1;
    const debtData = {
      id: new ObjectId(debt._id),
      valueFactor: factor
    };
    debtsData.push(debtData);
    total += factor * debt.value;
  });
  const { insertedId } = await paymentsRepository.insertOne({
    adminId,
    date: new Date(),
    debtsData,
    total
  });
  return insertedId.toHexString();
};
const getPaymentEnrichedList = (startDate, page, pagination) => {
  const opts = dataPagination(pagination, page, 'date');
  return paymentsRepository
    .findPaymentsEnriched(startDate, opts)
    .toArray();
};
module.exports = {
  createPayment,
  getPaymentEnrichedList
};
