const { ObjectId } = require('../infra/db');
const activitiesRepository = require('../repositories/activities-repository');
const dataPagination = require('../utils/data-pagination');
const activityToDTO = (activity) => {
  activity._id = activity._id.toHexString();
};
const createActivity = async (activityData) => {
  const { insertedId } = await activitiesRepository.insertOne(activityData);
  return insertedId.toHexString();
};
const deleteActivity = async (activityId) => {
  const { deletedCount } = await activitiesRepository.deleteOne(new ObjectId(activityId));
  if (!deletedCount) {
    return {
      error: new Error('Could not found the activity'),
      status: 404
    };
  }
  return;
};
const getActivities = async (pagination, page) => {
  const activitiesArray = await activitiesRepository.findByQuery({}, dataPagination(pagination, page), true);
  if (activitiesArray.length) {
    activitiesArray.forEach(a => activityToDTO(a));
  }
  return activitiesArray;
};
const getActivitiesNames = async () => {
  const activitiesArray = await activitiesRepository.findByQuery({}, {
    projection: {
      _id: -1,
      name: 1
    }
  }, true);
  return activitiesArray.map(a => a.name);
};
const getActivityByName = async (name) => {
  const activity = await activitiesRepository.findOneByQuery({
    name
  });
  if (activity) {
    activityToDTO(activity);
  }
  return activity;
};
const getActivityById = async (activityId) => {
  const activity = await activitiesRepository.findOneByQuery({
    _id: new ObjectId(activityId)
  });
  if (activity) {
    activityToDTO(activity);
  }
  return activity;
};
const updateActivityById = async (activityId, activityData) => {
  const { modifiedCount } = await activitiesRepository.setProperties({
    _id: new ObjectId(activityId)
  }, activityData);
  if (!modifiedCount) {
    return {
      error: new Error('Could not found the activity'),
      statusCode: 404
    };
  }
  return;
};
module.exports = {
  createActivity,
  deleteActivity,
  getActivities,
  getActivitiesNames,
  getActivityById,
  getActivityByName,
  updateActivityById
};
