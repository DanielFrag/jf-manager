const { DEFAULT_EXPIRE_DATE: defaultExpireDate } = require('../config/application-config');
const activitiesService = require('../services/activities-service');
const debtsService = require('../services/debt-service');
const clientsRepository = require('../repositories/clients-repository');
const createActivityDebt = async (clientId, activityName, refMonth, dueDate) => {
  const activity = await activitiesService.getActivityByName(activityName);
  if (!activity) {
    return;
  }
  const conflit = await debtsService.findClientDebtByActivityAndRefMonth(clientId, activity.name, refMonth);
  if (conflit) {
    return;
  }
  await debtsService.generateMonthlyDebt(clientId, activity, dueDate, refMonth);
};
const handleNextClient = async (cursor, refMonth) => {
  const client = await cursor.next();
  if (!client) {
    console.log('All monthly debts were generated');
    return;
  }
  const expireDate = client.debtsExpireDate || defaultExpireDate;
  const dueDate = new Date(`${refMonth}-${expireDate.toString().padStart(2, '0')}`);
  const promiseArray = client.activitiesData.map(activityData => createActivityDebt(
    client._id.toHexString(), activityData.name, refMonth, dueDate));
  await Promise.all(promiseArray);
  handleNextClient(cursor, refMonth);
};
const generateMonthlyDebts = (refMonth) => {
  const cursor = clientsRepository.findByQuery({
    status: 'active'
  });
  handleNextClient(cursor, refMonth);
};
module.exports = {
  generateMonthlyDebts
};
