const { ObjectId } = require('../infra/db');
const debtsReporitory = require('../repositories/debts-repository');
const debtToDTO = (debt) => {
  debt._id = debt._id.toHexString();
  debt.clientId = debt.clientId.toHexString();
  if (debt.paymentId) {
    debt.paymentId = debt.paymentId.toHexString();
  }
};
const findClientDebtByActivityAndRefMonth = async (clientId, activityName, referenceMonth) => {
  const debt = await debtsReporitory.findOneByQuery({
    clientId: new ObjectId(clientId),
    activityName: activityName,
    'details.referenceMonth': referenceMonth
  });
  if (debt) {
    debtToDTO(debt);
  }
  return debt;
};
const generateRegistrationDebt = async (clientId, registrationValue, daysToExpire) => {
  const now = new Date();
  const dueDate = new Date();
  dueDate.setDate(now.getDate() + daysToExpire);
  const debtData = {
    clientId: new ObjectId(clientId),
    details: {
      dueDate
    },
    generated: {
      adminId: 'system',
      date: now
    },
    type: 'registration',
    value: registrationValue,
    valid: true
  };
  await debtsReporitory.insertOne(debtData);
};
const generateMonthlyDebt = async (clientIdStr, activity, dueDate, referenceMonth) => {
  const activityName = activity.name;
  const clientId = new ObjectId(clientIdStr);
  const debtData = {
    clientId,
    details: {
      referenceMonth,
      dueDate: new Date(dueDate)
    },
    generated: {
      date: new Date()
    },
    activityName,
    type: 'monthly payment',
    value: activity.values.monthly,
    valid: true
  };
  const { insertedId } = await debtsReporitory.insertOne(debtData);
  return insertedId.toHexString();
};
const getDebtById = debtId => debtsReporitory.findOneByQuery({
  _id: new ObjectId(debtId)
});
const getDebtsByIds = async (debtsIdsSet, valid = false) => {
  const inData = [];
  debtsIdsSet.forEach(id => inData.push(new ObjectId(id)));
  const debts = await debtsReporitory
    .findByQuery({
      _id: {
        $in: inData
      },
      valid
    })
    .toArray();
  debts.forEach(d => debtToDTO(d));
  return debts;
};
const setPaymentToDebts = async (debtsIdsSet, paymentId) => {
  const idsArray = [];
  debtsIdsSet.forEach(id => idsArray.push(new ObjectId(id)));
  const { modifiedCount } = await debtsReporitory.updateManyWithSet({
    _id: {
      $in: idsArray
    }
  }, {
    paymentId: new ObjectId(paymentId)
  });
  return modifiedCount;
};
module.exports = {
  findClientDebtByActivityAndRefMonth,
  generateRegistrationDebt,
  generateMonthlyDebt,
  getDebtById,
  getDebtsByIds,
  setPaymentToDebts
};
