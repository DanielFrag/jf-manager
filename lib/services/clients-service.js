const { ObjectId } = require('../infra/db');
const dataPagination = require('../utils/data-pagination');
const clientRepository = require('../repositories/clients-repository');
const activitiesService = require('../services/activities-service');
const clientToDTO = (client) => {
  client._id = client._id.toHexString();
};
const createClient = async (name, birthDate, phone, cpf, debtsExpireDate) => {
  const clientObject = {
    birthDate,
    name,
    activitiesData: []
  };
  if (cpf) {
    clientObject.cpf = cpf;
  }
  if (phone) {
    clientObject.phone = phone;
  }
  if (debtsExpireDate) {
    clientObject.debtsExpireDate = debtsExpireDate;
  }
  const { insertedId } = await clientRepository.insertClient(clientObject);
  return insertedId.toHexString();
};
const getClientById = async (clientId) => {
  const client = await clientRepository.findOneByQuery({
    _id: new ObjectId(clientId)
  });
  if (client) {
    clientToDTO(client);
  }
  return client;
};
const getClients = async (page, pagination, sortFields) => {
  const opts = dataPagination(pagination, page, sortFields);
  const clientsArray = await clientRepository.findByQuery({}, opts, true);
  clientsArray.forEach(c => clientToDTO(c));
  return clientsArray;
};
const getClientByNameAndBirthDate = async (name, birthDate) => {
  const client = await clientRepository.findOneByQuery({
    name,
    birthDate
  });
  if (client) {
    clientToDTO(client);
  }
  return client;
};
const getClientsFullData = async (clientData, page, pagination, sortFields) => {
  const opts = dataPagination(pagination, page, sortFields);
  const clients = await clientRepository
    .getClientsWithDebtsAndPayments(clientData, opts)
    .toArray();
  return clients;
};
const getClientPositionForRegistrationNumber = () => clientRepository.getClientPosition({
  registrationNumber: {
    $gt: parseInt(`${new Date().getUTCFullYear()}0000`, 10)
  }
});
const patchClientPhoto = (clientId, photoBase64) => clientRepository.setClientProperties({
  _id: new ObjectId(clientId)
}, {
  photo: photoBase64
});
const pullClientActivity = async (clientId, activityName) => {
  const result = await clientRepository.pullActivity(new ObjectId(clientId), activityName);
  if (!result.modifiedCount) {
    return {
      error: new Error('Cannot found the requested client or activity'),
      statusCode: 404
    };
  }
};
const pushClientActivity = async (clientId, activityData) => {
  const activity = await activitiesService.getActivityByName(activityData.name);
  if (!activity) {
    return {
      error: new Error('Could not found any activity with the provided id'),
      statusCode: 400
    };
  }
  const { modifiedCount } = await clientRepository.pushActivityData(new ObjectId(clientId), activityData);
  if (!modifiedCount) {
    return {
      error: new Error('Could not found the client'),
      statusCode: 404
    };
  }
  return;
};
const updateClient = (clientId, updateData) => clientRepository.setClientProperties({
  _id: new ObjectId(clientId)
}, updateData);
const updateClientActivityCustomData = (clientId, activityName, customData) => {
  const cId = new ObjectId(clientId);
  if (!customData) {
    return clientRepository.unsetActivityCustomData(cId, activityName);
  }
  return clientRepository.pushActivityCustomData(cId, activityName, customData);
};
module.exports = {
  createClient,
  getClientById,
  getClientByNameAndBirthDate,
  getClientPositionForRegistrationNumber,
  getClients,
  getClientsFullData,
  patchClientPhoto,
  pullClientActivity,
  pushClientActivity,
  updateClient,
  updateClientActivityCustomData
};
