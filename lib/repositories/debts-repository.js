const { getCollection } = require('../infra/db');
const debtsCollection = () => getCollection('debts');
const findByQuery = (filter, options) => debtsCollection().find(filter, options);
const findOneByQuery = filter => debtsCollection().findOne(filter);
const insertOne = debtData => debtsCollection().insertOne(debtData);
const updateManyWithSet = (filter, setData) => debtsCollection().updateMany(filter, {
  $set: setData
});
module.exports = {
  findByQuery,
  findOneByQuery,
  insertOne,
  updateManyWithSet
};
