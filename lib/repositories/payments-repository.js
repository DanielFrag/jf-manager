const { getCollection } = require('../infra/db');
const paymentsCollection = () => getCollection('payments');
const findOneByQuery = filter => paymentsCollection().findOne(filter);
const insertOne = paymentData => paymentsCollection().insertOne(paymentData);
const findPaymentsEnriched = (startDate, opts) => {
  const yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);
  const query = {
    date: {
      $gte: startDate.valueOf() ? startDate : yesterday
    }
  };
  const pipeline = [{
    $match: query
  }];
  if (opts.sort) {
    pipeline.push({
      $sort: opts.sort
    });
  }
  pipeline.push(...[{
    $skip: opts.skip
  }, {
    $limit: opts.limit
  },
  {
    $lookup: {
      from: 'debts',
      let: {
        debtsIds: {
          $map: {
            input: '$debtsData',
            as: 'dData',
            in: '$$dData.id'
          }
        }
      },
      pipeline: [{
        $match: {
          $expr: {
            $in: ['$_id', '$$debtsIds']
          } 
        }
      }, {
        $lookup: {
          from: 'clients',
          localField: 'clientId',
          foreignField: '_id',
          as: 'client'
        }
      }, {
        $project: {
          client: {
            $arrayElemAt: ['$client', 0]
          },
          value: 1,
          type: 1,
          details: 1,
          activityName: 1
        }
      }, {
        $project: {
          'client.name': 1,
          'client.registrationNumber': 1,
          value: 1,
          type: 1,
          details: 1,
          activityName: 1
        }
      }],
      as: 'debts'
    }
  }]);
  return paymentsCollection().aggregate(pipeline);
};
module.exports = {
  findOneByQuery,
  findPaymentsEnriched,
  insertOne
};
