const { getCollection } = require('../infra/db');
const activityCollection = () => getCollection('activities');
const deleteOne = (activityId) => activityCollection().deleteOne({
  _id: activityId
});
const findByQuery = (filter, options, toArrayFlag) => {
  const cursor = activityCollection().find(filter, options);
  if (toArrayFlag) {
    return cursor.toArray();
  }
  return cursor;
};
const findOneByQuery = (filter) => activityCollection().findOne(filter);
const insertOne = activity => activityCollection().insertOne(activity);
const setProperties = (filter, updateData) => activityCollection().updateOne(filter, {
  $set: updateData
});
module.exports = {
  deleteOne,
  findByQuery,
  findOneByQuery,
  insertOne,
  setProperties
};
