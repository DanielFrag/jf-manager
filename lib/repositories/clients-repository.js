const { getCollection } = require('../infra/db');
const clientsCollection = () => getCollection('clients');
const findByQuery = (filter, options, toArrayFlag) => {
  const cursor = clientsCollection().find(filter, options);
  if (toArrayFlag) {
    return cursor.toArray();
  }
  return cursor;
};
const findOneByQuery = filter => clientsCollection().findOne(filter);
const getClientPosition = filter => clientsCollection().countDocuments(filter);
const insertClient = client => clientsCollection().insertOne(client);
const setClientProperties = (filter, propertiesObj, options) => clientsCollection().updateOne(filter, {
  $set: propertiesObj
}, options);
const pullActivity = (clientId, activityName) => clientsCollection().updateOne({
  _id: clientId
}, {
  $pull: {
    activitiesData: {
      name: activityName
    }
  }
});
const pushActivityData = (clientId, activityData) => clientsCollection().updateOne({
  _id: clientId
}, {
  $push: {
    activitiesData: activityData
  }
});
const pushActivityCustomData = (clientId, activityName, customData) => clientsCollection().updateOne({
  _id: clientId,
  'activitiesData.name': activityName
}, {
  $set: {
    'activitiesData.$.customData': customData
  }
});
const unsetActivityCustomData = (clientId, activityName) => clientsCollection().updateOne({
  _id: clientId,
  'activitiesData.name': activityName
}, {
  $unset: {
    'activitiesData.$.customData': ''
  }
});
const getClientsWithDebtsAndPayments = (filter, opts) => {
  const pipeline = [{
    $match: filter
  }];
  if (opts.sort) {
    pipeline.push({
      $sort: opts.sort
    });
  }
  pipeline.push(...[{
    $skip: opts.skip
  }, {
    $limit: opts.limit
  }, {
    $lookup: {
      from: 'debts',
      localField: '_id',
      foreignField: 'clientId',
      as: 'debts'
    }
  }, {
    $lookup: {
      from: 'payments',
      localField: 'debts.paymentId',
      foreignField: '_id',
      as: 'payments'
    }
  }]);
  return clientsCollection().aggregate(pipeline);
};
module.exports = {
  findByQuery,
  findOneByQuery,
  getClientPosition,
  getClientsWithDebtsAndPayments,
  insertClient,
  pullActivity,
  pushActivityData,
  pushActivityCustomData,
  setClientProperties,
  unsetActivityCustomData
};
