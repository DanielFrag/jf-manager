const fs = require('fs');
const path = require('path');
const tv4 = require('tv4');
let schemasLoaded;
const formatValidators = {};
formatValidators['year-month'] = (data) => {
  if (/^[0-9]{4}-[0-1][0-9]$/.test(data) && new Date(data).valueOf()) {
    return null;
  }
  return 'Date string does not match the format YYYY-MM';
};
formatValidators.date = (data) => {
  if (/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/.test(data) && new Date(data).valueOf()) {
    return null;
  }
  return 'Date string does not match the format YYYY-MM-DD';
};
formatValidators['date-time'] = (data) => {
  const dateReg = /^[0-9]{4}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]\.[0-9]{3}Z$/.test(data);
  if (dateReg && new Date(data).valueOf()) {
    return null;
  }
  return 'Date-time string does not match the format YYYY-MM-DDTHH:MM:SS.DDDZ';
};
formatValidators.cpf = (data) => {
  if (/^([0-9]{3}(\.?)){2}[0-9]{3}(-?)[0-9]{2}$/.test(data)) {
    return null;
  }
  return 'Invalid CPF string';
};
tv4.addFormat('year-month', formatValidators['year-month']);
tv4.addFormat('date', formatValidators.date);
tv4.addFormat('date-time', formatValidators['date-time']);
tv4.addFormat('cpf', formatValidators.cpf);
const schemasPath = path.resolve(__dirname, '../schemas');
const getSchemaFileNames = () => new Promise((resolve, reject) => {
  fs.readdir(schemasPath, (readError, files) => {
    return readError ? reject(readError) : resolve(files);
  });
});
const loadTv4Schema = (fileName) => new Promise((resolve, reject) => {
  fs.readFile(`${schemasPath}/${fileName}`, 'utf8', (readError, fileString) => {
    if (readError) {
      return reject(readError);
    }
    tv4.addSchema(JSON.parse(fileString));
    resolve();
  });
});
module.exports = {
  validateSchema(data, schemaId, strictValidation) {
    const result = tv4.validateMultiple(data, schemaId, false, strictValidation);
    return result.valid ? {
      valid: true
    } : {
      valid: false,
      errors: result.errors.map(e => ({
        message: e.message,
        path: e.schemaPath || e.dataPath
      }))
    };
  },
  loadSchemas() {
    if (schemasLoaded) {
      return Promise.resolve();
    }
    return getSchemaFileNames()
      .then((files) => files.reduce((t, c) => t.then(() => loadTv4Schema(c)), Promise.resolve()))
      .then(() => {
        schemasLoaded = true;
      });
  },
  validateFormat(data, formatId) {
    if (!formatValidators[formatId]) {
      return {
        error: new Error('Could not found the requested format')
      };
    }
    const result = formatValidators[formatId](data);
    if (result) {
      return {
        valid: false,
        error: new Error(result)
      };
    }
    return {
      valid: true
    };
  }
};
