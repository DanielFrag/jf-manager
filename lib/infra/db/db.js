const { MongoClient, ObjectId } = require('mongodb');
function Db() {
  this.collections = new Map();
  this.client = null;
  this.db = null;
  this.ObjectId = ObjectId;
}
Db.prototype.close = async function() {
  if (!this.client) {
    return;
  }
  await this.client.close();
  this.client = null;
  this.collections.clear();
  this.db = null;
};
Db.prototype.connect = async function(uri, options) {
  if (this.client) {
    return;
  }
  this.client = await MongoClient.connect(uri, options);
  this.db = this.client.db();
};
Db.prototype.getCollection = function(collectionName) {
  let collection = this.collections.get(collectionName);
  if (!collection) {
    collection = this.db.collection(collectionName);
    this.collections.set(collectionName, collection);
  }
  return collection;
};
module.exports = Db;
