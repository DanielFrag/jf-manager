const Db = require('./db');
const dbInstance = new Db();
module.exports = {
  close() {
    return dbInstance.close();
  },
  connect(uri, options = {}) {
    if (!dbInstance.client) {
      return dbInstance.connect(uri, Object.assign(options, {
        useUnifiedTopology: true,
        useNewUrlParser: true
      }));
    }
  },
  getCollection(collectionName) {
    if (!collectionName) {
      return;
    }
    return dbInstance.getCollection(collectionName);
  },
  ObjectId: dbInstance.ObjectId
};
