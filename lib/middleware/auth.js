const { USER_NAME, USER_PASS } = require('../config/application-config');
const authorizedUser = Buffer.from(`${USER_NAME}:${USER_PASS}`).toString('base64');
module.exports = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.sendStatus(401);
  }
  const [, authData] = authorization.split(' ');
  if (authData !== authorizedUser) {
    return res.sendStatus(401);
  }
  next();
};
