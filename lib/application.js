const db = require('./infra/db');
const app = require('./config/express-config');
const applicationConfig = require('./config/application-config');
const validation = require('./validation');
let serverInstance;
module.exports = {
  async startApplication() {
    await validation.loadSchemas();
    console.log('Schemas loaded');
    await db.connect(applicationConfig.MONGO_URI);
    console.log('DB connected');
  },
  startListen() {
    return new Promise((resolve, reject) => {
      serverInstance = app.listen(app.get('port'), (error) => {
        if (error) {
          return reject(error);
        }
        console.log(`Started on port: ${app.get('port')}`);
        resolve();
      });
    });
  },
  async closeApplication() {
    await new Promise((resolve) => {
      if (serverInstance.listening) {
        return serverInstance.close(resolve);
      }
      resolve();
    });
    serverInstance = null;
    await db.close();
    console.log('DB closed');
  }
};
